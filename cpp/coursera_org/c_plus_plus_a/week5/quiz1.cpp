#include <iostream>

using namespace std;

int main()
{
    int a = 2, b = -3, c = 2;
    bool tval1 = false, tval2 = true;
    char ch = 'b';

    auto res1 = b+c*a;
    cout << "res1: " << res1 << endl; // 1
    auto res2 = c % 5/2;
    cout << "res2: " << res2 << endl; // 1
    auto res3 = a * c++;
    cout << "res3: " << res3 << endl; // 4
    bool res4 = tval1 && tval2;
    cout << "res4: " << res4 << endl; // false
    char res5 = ch+2;
    cout << "res5: " << res5 << endl; // d
    return 0;
}
