#include <iostream>

using namespace std;

template <class T>
inline void myswap(T& d, T&s)
{
    T temp;
    temp = d;
    d = s;
    s = temp;
}

int main()
{
    double a = 3.24, b = 11.25;
    float c = 30.24, d = 110.25;
    cout << "a: " << a << " - b: " << b << endl;
    myswap<double>(a, b);
    cout << "a: " << a << " - b: " << b << endl;

    cout << "c: " << c << " - d: " << d << endl;
    //myswap<float>(c, d);
    myswap(c, d);
    cout << "c: " << c << " - d: " << d << endl;
}
