// 
// Compute the sum of all integers from 0 to N
// N is a const value defined at compile time
// 
// compile with: g++ -Wall 02_exo01.cpp -o 02_exo01_cpp
// run with: ./02_exo01_cpp

#include <iostream>
#include <vector>

using namespace std;

const int N = 40;

inline void sum(int &p, const vector<int> &d)
{
    // sum values of vector d into p

    p = 0;
    for(size_t i=0; i<d.size(); ++i)
    {
        p += d[i];
    }
}

int main()
{
    int accum = 0;
    // set data to a vector of size N
    // then fill the vector
    vector<int> data(N);
    for(int i=0; i<N; ++i)
    {
        data[i] = i;
    }

    // sum and print result
    sum(accum, data);
    cout << "sum is " << accum << endl;
    // return success
    return 0;
}


