#include <stdio.h>

void swap(int *i, int *j)
{
    int temp = *i;
    *i = *j;
    *j = temp; 
}

void swapd(double *i, double *j)
{
    double temp = *i;
    *i = *j;
    *j = temp; 
}

int main(int argc, char **argv)
{
    int a = 5;
    int b = 15;
    double c = 3.45;
    double d = 6.45;
    printf("a: %d, b: %d\n", a, b);
    printf("c: %lf, d: %lf\n", c, d);
    swap(&a, &b);
    swapd(&c, &d);
    printf("a: %d, b: %d\n", a, b);
    printf("c: %lf, d: %lf\n", c, d);
}
