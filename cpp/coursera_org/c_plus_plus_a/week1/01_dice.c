// include
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// pre processor
#define SIDES 6
#define RANDOM_SIDE (rand() % SIDES + 1)
#define DICE_NUMBER 2

// main entry function
int main(int argc, char **argv)
{
    int j, trials;
    int *outcomes;
    // alloc mem
    outcomes = (int *)malloc((DICE_NUMBER * SIDES + 1) * sizeof(int));

    // printf("SIDES %d\n", SIDES);
    // printf("RANDOM_SIDE %d\e", RANDOM_SIDE);
    
    // seeds
    srand(clock());
    printf("\nEnter number of trials:");
    scanf("%d", &trials);
    
    // simulate dice rolling
    printf("Going for %d trials\n", trials);
    for(j=0; j<trials; j++)
    {
        int d1 = RANDOM_SIDE;
        int d2 = RANDOM_SIDE;
        // printf("d1: %d - d2: %d, d1+d2: %d\n", d1, d2, d1+d2);
        outcomes[d1+d2]++;
    }

    printf("Probability\n");
    for(j=2; j<DICE_NUMBER * SIDES +1; j++)
    {
        printf("j = %d, outcomes = %d, p = %lf\n", j,
                outcomes[j],
                (double)(outcomes[j]/(double)trials));
    }

    // free mem
    free(outcomes);
    // all good!
    return 0;
}
