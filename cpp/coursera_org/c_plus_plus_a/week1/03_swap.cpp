// #include <stdio.h>

#include <iostream>

using namespace std;

inline void swap(int *i, int *j)
{
    int temp = *i;
    *i = *j;
    *j = temp; 
}

inline void refswap(int &i, int &j)
{
    int temp = i;
    i = j;
    j = temp;
}

// overloading swap with double arguments
inline void swap(double *i, double *j)
{
    double temp = *i;
    *i = *j;
    *j = temp; 
}

inline void refswap(double &i, double &j)
{
    double temp = i;
    i = j;
    j = temp;
}

int main(int argc, char **argv)
{
    int a = 5;
    int b = 15;
    double c = 3.45;
    double d = 6.45;
    // printf("a: %d, b: %d\n", a, b);
    // printf("c: %lf, d: %lf\n", c, d);
    
    cout << "a: " << a << " b: " << b << endl;
    cout << "c: " << c << " d: " << d << endl;

    swap(&a, &b);
    swap(&c, &d);

    // This also works by why?
    // swap(a, b);
    // swap(c, d);
    
    // printf("a: %d, b: %d\n", a, b);
    // printf("c: %lf, d: %lf\n", c, d);
    
    cout << "a: " << a << " b: " << b << endl;
    cout << "c: " << c << " d: " << d << endl;

    refswap(a, b);
    refswap(c, d);

    cout << "a: " << a << " b: " << b << endl;
    cout << "c: " << c << " d: " << d << endl;

}
