#include <stdio.h>
#include <iostream>

using namespace std;

#define N 40

void sum_c(int *p, int n, int d[])
{
    int i = 0;
    *p = 0;
    for(i=0; i<n; i++)
    {
        *p = *p + d[i];
    }
}

template<class T>
void tsum(T &p, int n, T d[])
{
    p = 0;
    for(int i=0; i<n; i++)
    {
        p += d[i];
    }
}

int main(int argc, char **argv)
{
    int i=0;
    int acc=0;
    int array[N];
    for(i=0; i<N; i++)
    {
        array[i] = i;
    }

    sum_c(&acc, N, array);
    printf("sum of number from 0 to %d: %d\n", N, acc);

    tsum(acc, N, array);
    printf("sum of number from 0 to %d: %d\n", N, acc);

    double dacc = 0;
    double darray[N];
    for(i=0; i<N; i++)
    {
        darray[i] = i + 0.7;
    }

    tsum(dacc, N, darray);
    // printf("sum of doube number from 0 to %d (+.7 each): %d", N, acc);
    cout << "sum of double number (+0.7 each) from 0 to " << N << " is " << dacc << endl;

    return 0;
}


