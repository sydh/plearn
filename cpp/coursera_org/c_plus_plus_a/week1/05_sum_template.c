#include <stdio.h>

#define N 40

void sum_c(int *p, int n, int d[])
{
    int i = 0;
    *p = 0;
    for(i=0; i<n; i++)
    {
        *p = *p + d[i];
    }
}


int main(int argc, char **argv)
{
    int i=0;
    int acc=0;
    int array[N];
    for(i=0; i<N; i++)
    {
        array[i] = i;
    }

    sum_c(&acc, N, array);
    printf("sum of number from 0 to %d: %d", N, acc);

    return 0;
}


