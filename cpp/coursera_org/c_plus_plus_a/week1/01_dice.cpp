#include <iostream> // io pkg
#include <cstdlib> // stdlib from C
#include <ctime> // time from C

using namespace std;

// replace #define
const int sides = 6;
// inline? -> optim this func
inline int r_sides() { return (rand() % sides + 1); }

int main(int argc, char **argv)
{
    const int n_dice = 2;
    int d1, d2;
    srand(clock());
    // cout << "\nEnter number of trials:" << endl;
    cout << endl << "Enter number of trials:" << endl; // ~ printf
    // or: std::cout << ....
    // :: is the scope operator
    // << is the overload operator
    // cout is of type ostream
    int trials; // interleave declaration
    cin >> trials; // ~ scanf but type safe -- cin is istream type

    // ~ malloc - mem alloc'ed on the heap
    int *outcomes = new int[n_dice * sides + 1];
    // delete outcome;

    for(int j=0; j<trials; j++)
    {
        d1 = r_sides(), d2 = r_sides();
        outcomes[d1+d2]++;
    }
    for(int j=2; j<n_dice * sides + 1; j++)
    {
        // ~ (TYPE) in C (unsafe) but static_cast => safe conversion 
        double _outcomes = static_cast<double>(outcomes[j]);
        double _trials = static_cast<double>(trials);
        cout << "j= " << j << " p= " 
            << _outcomes/_trials
            // iomanip, ~ \n
            << endl;
    }





}
