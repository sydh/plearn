// coursera 
// week4 
// implement minimum spanning tree (mst) algo
//
// Compile with:
// clang++ -Wall -Wextra 09_mst.cpp -o 09_mst
// or with:
// g++ -Wall -Wextra 09_mst.cpp -o 09_mst

// run:
// ./09_mst
//
// Notes
// - Keep everything in 1 cpp file for easy submission
// - Implemented mst algo from Jarnik Prim
// - Implemented mst algo from Kruskal

#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <queue>
#include <cassert>
#include <string>
#include <fstream>

#include <map>
#include <set>

using namespace std;

enum class MST_ALGO:int { JARNIK_PRIM, KRUSKAL };

template <typename T>
void print_map(T& m)
{
    // debug function to print std::map content
    for(auto p: m)
        cout << p.first << " - " << p.second << endl;
}

template <typename T>
void print_set(T& s)
{
    // debug function to print std::set content
    for(auto p: s)
        cout << p << endl;
}

class DisjointSet
{
public:
    DisjointSet(int set_count=0);
    int set_count() { return _set_count; };
    
    void make_set(int set_index, int value);
    void compress_set(int set_index1, int set_index2);
    int find_set(int index);
private:
    vector<set<int>> data;
    map<int, int> set_map;
    int _set_count;
};

DisjointSet::DisjointSet(int set_count)
{
    // Initialize DisjointSet with set count
    // A Disjoint set is a data structure that tracks a set of elements 
    // partitioned into a number of disjoint (non-overlapping) subsets
    //
    // Note: this was implemented very naively (see Wikipedia article for
    // better implementation)

    _set_count = set_count;
    data.resize(set_count);
}

void DisjointSet::make_set(int set_index, int value)
{
    // init set at set_index in DisjointSet with value
    data[set_index].insert(value);
    set_map.insert(make_pair(value, set_index));
}

int DisjointSet::find_set(int value)
{
    // find set_index from value
    auto it = set_map.find(value); 
    if(it!=set_map.end())
        return it->second;
    else
        return -1;
}

void DisjointSet::compress_set(int set_index1, int set_index2)
{
    // add values in set @ index2 into set @ index1
    // clear set @ index2
    auto &s1 = data[set_index1];
    auto &s2 = data[set_index2];

    // merge s1 into s2 && update set_map
    for(auto v: s2)
    {
       s1.insert(v);
       set_map[v] = set_index1;
    }

    // clear merged set
    s2.clear();
    _set_count--;
}

class Graph
{
public:
    Graph(int);
    Graph(string);
    int node_count();
    int edge_count();
    double edge_value(int); 
    double edge_value(int, int);
    void set_edge_value(int, int, double);
    size_t size();
    void dump();
    
    void edge_to_nodes(int edge, int &node_src, int &node_dst);
    int nodes_to_edge(int node_src, int node_dst);
    int neighbours(int node, vector<int> &neighbours);
    double mst(int, vector<int> &, MST_ALGO);

private:

    double mst_jp(int, vector<int> &);
    double mst_kal(vector<int> &);

    int _node_count;
    int _edge_count;
    vector<double>graph_data;
};

void Graph::edge_to_nodes(int edge, int &node_src, int &node_dst)
{
    // from an edge index, return source and destination nodes index
    // edges are defined as row -> column
    node_src = edge / _node_count;
    node_dst = edge % _node_count;
}

int Graph::nodes_to_edge(int node_src, int node_dst)
{
    // from source and dest nodes index, return edge index
    // edges are defined as row -> column
    return node_src*_node_count+node_dst;
}

int Graph::neighbours(int node, vector<int> &neighbours) 
{
    // return neighbours of given node index (node dst indexes)
    // neighbour count is returned, neighbours in neighbours vector
    // Note: neighbours vector size >= neighbour count
    // Note: vector neighbours can be resized if too small

    // set neighbours to the maximum of neighbours
    if(neighbours.size() < static_cast<size_t>(_node_count - 1))
        neighbours.resize(_node_count - 1);
    
    int nei_count = 0;

    // iterate over row
    for(int i=0; i<_node_count; i++)
    {
        if(graph_data[node*_node_count+i] > 0.0)
        {
            // neighbours[nei_count] = node*_node_count+i;
            neighbours[nei_count] = i;
            nei_count += 1;
        }
    }

    return nei_count;
}

Graph::Graph(int node_count) 
{
    // Graph constructor
    // only number of node is specified

    this->_node_count = node_count;
    this->_edge_count = 0;
    graph_data.assign(node_count*node_count, 0);
}

Graph::Graph(string path)
{
    // Graph constructor
    // read graph data from file
    // file format is:
    // line 0: node count
    // line 1...n: node src (int), node dst (int), cost (int)

    //cout << "Reading " << path << endl;
    std::ifstream infile(path);   
    int node_count=0, node_src=0, node_dst=0, cost=0;
    
    infile >> node_count;
    this->_node_count = node_count;
    this->_edge_count = 0;
    graph_data.assign(node_count*node_count, 0);
    
    while (infile >> node_src >> node_dst >> cost)
    {
        //cout << "adding " << node_src << " - " << node_dst << " - " << cost << endl;
        this->set_edge_value(node_src, node_dst, static_cast<double>(cost)); 
    }
}

int Graph::node_count()
{
    // get number of nodes in graph
    return _node_count;
}

int Graph::edge_count()
{
    // get number of edges in graph
    return _edge_count;
}

double Graph::edge_value(int edge)
{
    // get cost associated to edge index
    return graph_data[edge];
}

double Graph::edge_value(int node_src, int node_dst)
{
    // get cost associated to edge going from src to dst nodes
    int edge = nodes_to_edge(node_src, node_dst); 
    return graph_data[edge];
}

void Graph::set_edge_value(int node_src, int node_dst, double value)
{
    // Set edge value (eg. cost) to go from src to dest nodes
    // TODO: could warn if node_src & node_dst are out of bounds for current graph

    // unidirectional graph so same value for both direction
    int edge1 = nodes_to_edge(node_src, node_dst);
    int edge2 = nodes_to_edge(node_dst, node_src);

    if(graph_data[edge1] == 0 && graph_data[edge2] == 0)
        _edge_count += 1;

    graph_data[edge1] = value;
    graph_data[edge2] = value;
}

void Graph::dump()
{
    // TODO: improve this with width instead of double precision
    
    // Save flags/precision.
    std::ios_base::fmtflags oldflags = std::cout.flags();
    std::streamsize oldprecision = std::cout.precision();

    std::cout << std::fixed << std::setprecision(3);
    for(int i=0; i<_node_count; i++)
    {
        for(int j=0; j<_node_count; j++)
        {
            cout << edge_value(i, j) << " ";
        }
        cout << endl;
    }

    // Restore flags/precision.
    std::cout.flags(oldflags);
    std::cout.precision(oldprecision);
    
}

size_t Graph::size()
{
    // get size of graph (eg. underlying container size)
    // multiply this value with sizeof(double) to get the size in bytes 
    return graph_data.size();
}

double Graph::mst_jp(int node, vector<int> &mst_edges)
{
    // min spanning tree algo (Jarnik Prim) 

    double cost = 0.0;

    // init before loop
    vector<int> nei(_node_count);
    int node_in_mst = 1;
    vector<int>edge_seen;
    edge_seen.reserve(_edge_count);
    vector<int>node_seen;
    node_seen.reserve(_node_count);

    // lambda function: for sorting pair in priority_queue
    auto cmp_pair = [](pair<int, double> &p1, pair<int, double> &p2) { return p1.second > p2.second; };
    priority_queue<pair<int, double>, vector<pair<int, double>>, decltype(cmp_pair)> s2(cmp_pair);
    
    s2.push(make_pair(nodes_to_edge(node, node), 0)); // assume cost 0 for loop

    do 
    {
        const auto &edge_dist = s2.top();
        
        int edge = edge_dist.first;
        double _cost = edge_dist.second;

        s2.pop();

        int _node_src, _node_dst;
        edge_to_nodes(edge, _node_src, _node_dst);

        int edge2 = nodes_to_edge(_node_dst, _node_src);

        // check if edges not already in set (edge_seen)
        std::vector<int>::iterator it;
        it = find(edge_seen.begin(), edge_seen.end(), edge);
        if (it != edge_seen.end())
            continue; // found edge1

        it = find(edge_seen.begin(), edge_seen.end(), edge2);
        if (it != edge_seen.end())
            continue; // found edge2

        // check if node has not already been reached
        it = find(node_seen.begin(), node_seen.end(), _node_dst);
        if (it != node_seen.end())
            continue;

        // all good

        node_seen.push_back(_node_dst);
        edge_seen.push_back(edge);
        node_in_mst++;
        //cout << "node in mst " << node_in_mst << endl;
        //cout << "adding edge " << edge << " node " << _node_src << " - " << _node_dst << " cost " << _cost << endl;
        cost += _cost; 
        mst_edges.push_back(edge);
        
        int nei_count = neighbours(_node_dst, nei);
        //cout << "nei count for node " << _node_dst << endl;
        for(int i=0; i<nei_count; i++)
        {
            int edge = nodes_to_edge(_node_dst, nei[i]); 
            double edge_value = this->edge_value(edge);
            s2.push(make_pair(edge, edge_value));
        }
        
    } while(node_in_mst <= _node_count && !s2.empty());

    return cost;
}

double Graph::mst_kal(vector<int> &mst_edges)
{
    // init data(s)

    double cost = 0.0;

    // priority queue for edge_index, edge_value
    // lambda function: for sorting pair in priority_queue
    auto cmp_pair = [](pair<int, double> &p1, pair<int, double> &p2) { return p1.second > p2.second; };
    priority_queue<pair<int, double>, vector<pair<int, double>>, decltype(cmp_pair)> s2(cmp_pair);

    // in priority queue, add only edge indexes for unidir graph + no loop
    int max_edge_index = _node_count * (_node_count + 1) / 2 - _node_count;
    int current_col = 1;
    int current_row = 0;
    for(int i=0; i<max_edge_index; i++)
    {
        int edge = current_col+current_row*_node_count;
        double _cost = edge_value(edge);
        if(_cost>0.0)
        {
            //cout << "pushing " << edge << " - " << _cost << endl;
            s2.push(make_pair(edge, _cost));
        }

        current_col++;
        if(current_col >= _node_count)
        {
            current_row++;
            current_col = current_row+1;
        }
    }

    // disjoint set
    DisjointSet ds(_node_count);
    // each node of the graph goes in 1 set
    for(int i=0; i<_node_count; i++)
    {
        ds.make_set(i, i);
    }

    bool done = false;
    do
    {
        //
        // pop edge with min value
        
        const auto &edge_dist = s2.top();
        int edge = edge_dist.first;
        double _cost = edge_dist.second;

        //cout << "Checking edge " << edge << " - " << _cost << endl;

        int _node_src;
        int _node_dst;
        edge_to_nodes(edge, _node_src, _node_dst);
        
        s2.pop();

        // check for loops 
        int set_index1 = ds.find_set(_node_src);
        int set_index2 = ds.find_set(_node_dst);

        if(set_index1 != set_index2)
        {
            ds.compress_set(set_index1, set_index2);
            //cout << "Adding edge " << edge << " - " << _cost << endl;
            cost += _cost;
            mst_edges.push_back(edge);
        }

        // TODO: can also test if ds.set_count() == 1?
        done = s2.empty();

    } while(!done);

    return cost;

}

double Graph::mst(int node, vector<int> &mst_edges, MST_ALGO algo=MST_ALGO::JARNIK_PRIM)
{
    if(algo == MST_ALGO::JARNIK_PRIM)
        return mst_jp(node, mst_edges);
    else // if(algo == MST_ALGO::KRUSKAL) 
        return mst_kal(mst_edges);
}

void tests()
{
    // unit tests
    
    // file empty
    Graph g_empty("./cplusplus4c_empty");
    assert(g_empty.node_count() == 0);
    assert(g_empty.edge_count() == 0);
    // broken file with 1 edge with cost value is: AA
    Graph g_broken("./cplusplus4c_broken");
    assert(g_broken.node_count() == 11);
    assert(g_broken.edge_count() == 0);
    
    Graph g0("./cplusplus4c_graph_video_4_2");
    // A: 0, E: 1, F: 2
    // G: 3, D: 4, I: 5
    // H: 6
    assert(g0.node_count() == 7);
    assert(g0.edge_count() == 11);

    int start_node = 0;
    vector<int>result;
    result.reserve(g0.edge_count());
    double g0_mst0_cost = g0.mst(start_node, result);
    cout << "g0 mst prim cost " << g0_mst0_cost << endl;
    assert(static_cast<int>(g0_mst0_cost) == 19);

    result.clear();
    double g0_mst1_cost = g0.mst(start_node, result, MST_ALGO::KRUSKAL);
    cout << "g0 mst kruskal cost " << g0_mst1_cost << endl;
    assert(static_cast<int>(g0_mst1_cost) == 19);

    // disjoint set tests

    DisjointSet ds1(3);

    ds1.make_set(0, 11);
    ds1.make_set(1, 4);
    ds1.make_set(2, 7);

    assert(ds1.set_count() == 3);
    
    int v1 = 11;
    int v2 = 4;
    int v3 = 7;
    assert(ds1.find_set(v1) == 0);
    assert(ds1.find_set(v2) == 1);
    assert(ds1.find_set(v3) == 2);
    // compress set 0 into set 1
    ds1.compress_set(1, 0);
    assert(ds1.set_count() == 2);
    
    assert(ds1.find_set(v1) == 1);
    assert(ds1.find_set(v2) == 1);
    assert(ds1.find_set(v3) == 2);
}

int main()
{
    // unit tests
    
    tests();

    // main code
    Graph g1("./cplusplus4c_homeworks_Homework3_SampleTestData_mst_data");
    assert(g1.node_count() == 20);
    assert(g1.edge_count() == 344/2);
    
    int start_node = 0;
    vector<int>result;
    result.reserve(g1.edge_count());
    double g1_mst0_cost = g1.mst(start_node, result, MST_ALGO::JARNIK_PRIM);
    cout << "graph minimum spanning tree cost (prim): " << g1_mst0_cost << endl;
    cout << "edges for mst: " << endl;
    for(auto edge: result)
    {
        cout << "edge " << edge << endl;
    }

    result.clear();
    double g1_mst1_cost = g1.mst(start_node, result, MST_ALGO::KRUSKAL);
    cout << "graph minimum spanning tree cost (kruskal): " << g1_mst1_cost << endl;
    cout << "edges for mst: " << endl;
    for(auto edge: result)
    {
        cout << "edge " << edge << endl;
    }

    // return SUCCESS
    return 0;
}
