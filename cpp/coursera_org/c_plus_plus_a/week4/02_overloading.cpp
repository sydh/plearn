#include <iostream>

using namespace std;

class Point
{
public:
    
    Point(double x, double y): x(x), y(y) {}
    
    // Note: using explicit prevent user from 
    // implicit conversion
    // Point(double u): x(u), y(0.0) {
    explicit Point(double u): x(u), y(0.0) {
        cout << "conv from double..." << endl;
    }
    void print(ostream &out) const
    {
        out << x << " - " << y;
    }
    // Warning: maybe not a good idea
    // Converting a Point to a double is not obvious
    // operation
    operator double();

    // for Point + Point
    Point operator+(Point &p);
    
private:
    double x, y;
};

Point::operator double()
{
    return x*x+y*y;
}

Point Point::operator+(Point &p)
{
    return Point(x+p.x, y+p.y);
}

// debug

ostream &operator<<(ostream & out, const Point &p)
{
    p.print(out);
    return out;
}

int main()
{
    double d = 3.5;
    // implicit conversion not allowed here
    // Point p = d;
    Point p = static_cast<Point>(d);
    cout << "point " << p << endl;

    double d1 = p;
    cout << "d1 " << d1 << endl;

    // +
    
    Point pa(1, 2);
    Point pb(3, 4);

    cout << "pa + pb: " << pa + pb << endl;
    cout << "pa + pb: " << pa.operator+(pb) << endl;

    return 0;
}
