#include <iostream>

// container
#include <vector>

using namespace std;

int main()
{

    vector<int> v(100);

    // C style iteration
    for(int i=0; i<100; ++i)
    {
        v[i] = i;
    }

    // C++ style iteration
    for(vector<int>::iterator p=v.begin();
            p!=v.end(); ++p)
    {
        cout << *p << '\t';
    }
    cout << endl;

    return 0;
}
