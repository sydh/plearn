#include <iostream>
#include <vector>
#include <set>
#include <map>

using namespace std;

class DisjointSet
{
public:
    DisjointSet(int set_count=0);
    int set_count() { return _set_count; };
    
    void make_set(int set_index, int value);
    void compress_set(int set_index1, int set_index2);
    int find_set(int value);
private:
    //vector<set<int>> data;
    map<int, int> set_map;
    int _set_count;
};

DisjointSet::DisjointSet(int set_count)
{
    // init DisjointSet with set count
    // WARNING: assume all values in sets are unique
    // This allow to maintain a map of value -> set index
    // and easily find in which set a value is

    _set_count = set_count;
    //data.resize(set_count);
}

void DisjointSet::make_set(int set_index, int value)
{
    // init set at set_index in DisjointSet with value
    //data[set_index].insert(value);
    set_map.insert(make_pair(value, set_index));
}

int DisjointSet::find_set(int value)
{
    // find set_index from value
    auto it = set_map.find(value); 
    if(it!=set_map.end())
        return it->second;
    else
        return -1;
}

void DisjointSet::compress_set(int set_index1, int set_index2)
{
    // add values in set @ index2 into set @ index1
    // clear set @ index2
    //auto &s1 = data[set_index1];
    //auto &s2 = data[set_index2];

    // merge s1 into s2 && update set_map
    /*
    for(auto v: s2)
    {
       s1.insert(v);
       set_map[v] = set_index1;
    }
    */

    bool set2_empty = true;
    for(auto &p: set_map)
    {
        if(p.second == set_index2)
        {
            set2_empty = false;
            set_map[p.first] = set_index1;
        }
    }

    // clear merged set
    //s2.clear();
    if(set2_empty)
    {
        _set_count--;
    }
}

int main()
{   
    DisjointSet ds1(3);

    ds1.make_set(0, 11);
    ds1.make_set(1, 4);
    ds1.make_set(2, 7);

    int v1 = 11;
    cout << "find set index for value " << v1 << " - " << ds1.find_set(v1) << endl;
    int v2 = 4;
    cout << "find set index for value " << v2 << " - " << ds1.find_set(v2) << endl;

    ds1.compress_set(1, 0);
    cout << "find set index for value " << v1 << " - " << ds1.find_set(v1) << endl;
    cout << "find set index for value " << v2 << " - " << ds1.find_set(v2) << endl;

    return 0;
}
