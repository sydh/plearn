#include <iostream>
#include <iterator>
#include <fstream>
#include <vector>
#include <numeric>

using namespace std;

int main()
{
    ifstream data_file("data.txt");
    istream_iterator<int> start(data_file), end;
    vector<int> data(start, end);
    for(vector<int>::iterator it=data.begin(); it!=data.end();
            ++it)
    {
        cout << "item " << *it << endl;
    }

    int sum = accumulate(data.begin(), data.end(), 0);

    // or
    double sum2 = 0.0;
    for(auto d: data)
    {
        sum2 += d;        
    }

    cout << "sum " << sum << " - " << sum2 << endl;

    // use of auto to modify items
    for(auto &d: data)
    {
        d += 2;
    }
    
    for(auto d: data)
    {
        cout << "item " << d << endl;
    }

    return 0;
}
