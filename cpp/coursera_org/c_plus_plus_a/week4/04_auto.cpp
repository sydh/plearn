#include <iostream>

// container
#include <vector>

// Warning: add RTTI to program
// https://en.wikipedia.org/wiki/Run-time_type_information
// on my system, add 8k (from 16ko to 24ko)
#include <typeinfo>

using namespace std;

int main()
{
    auto i = 3;
    auto d = 3.7;
    auto c = d;

    cout << "type i " << typeid(i).name() << endl;
    cout << "type d " << typeid(d).name() << endl;

    cout << "type c " << typeid(c).name() << endl;

    //vector<int> v(100);
    vector<int> v(100, -1);

    // C++ style iteration with auto
    for(auto p = v.begin(); p!=v.end(); ++p)
    {
        cout << *p << '\t';
    }
    cout << endl;

    return 0;
}
