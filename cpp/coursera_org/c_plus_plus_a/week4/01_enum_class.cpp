#include <iostream>

using namespace std;

// C style enum
enum cColor {cRED=0, cGREEN, cBLUE};
enum cTrafic {tRED, tORANGE, tGREEN};

// C++ style enum (class enum)
enum class Color {RED, GREEN, BLUE};
enum class Traffic {RED, ORANGE, GREEN};

// class enum with type
enum class Traffic_status:bool {OFF=0, ON};

ostream &operator<<(ostream &out, Color &c)
{
    switch(static_cast<int>(c))
    {
        case 0:
            out << "red";
            break;
        case 1:
            out << "green";
            break;
        case 2:
            out << "blue";
            break;
    }
    return out;
}

int main()
{
    Color c1 = Color::RED;
    cout << "color c1 " << c1 << endl;
    Traffic tl_street_0 = Traffic::RED;
    Traffic_status tlst_street_0 = Traffic_status::OFF;

    cout << "trafic light for street 0: " << static_cast<int>(tl_street_0) << " - st: " << static_cast<bool>(tlst_street_0) << endl;
    
    // Note: advantage of enum class is that it's strongly type
    //cout << "trafic red == color red" << (tl_street_0 == c1) << endl;
    
    cColor cc1 = cColor::cRED;
    cTrafic ctl1 = cTrafic::tRED;

    // Note: with C style enum, comparison is allowed
    // but generate a warning @ compile time
    cout << "Comparison of cColor versus cTrafic: " << (cc1 == ctl1) << endl;

    return 0;
}
