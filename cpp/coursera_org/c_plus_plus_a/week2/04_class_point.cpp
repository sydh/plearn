#include <iostream>

using namespace std;

class Point
{
public:
    Point() { x=0; y=0; };
    Point(double, double);
    // Note: should declare accessor as const
    // Compiler knows that func does not modify object
    double getx() const { return x; };
    double gety() const { return y; };
    // Implement operator + in Class def
    Point operator+(const Point &p1) {
        Point result;
        result.x = x + p1.x;
        result.y = y + p1.y;
        return result;
    }
    // Implement outside class def
    Point operator-(const Point &);
    // Need to declare print as const
    // as operator overloading << will pass a const
    void print(ostream &out) const
    {
        out << x << " || " << y;
    }
private:
    double x, y;
};

Point::Point(double x, double y)
{
    this->x = x;
    this->y = y;
}

Point Point::operator-(const Point &p1)
{
    Point result;
    result.x = x - p1.x;
    result.y = y - p1.y;
    return result;
}

ostream &operator<<(ostream &out, const Point &p)
{
    // 2 ways to implement this func
    // use accessor
    // or use print (public) functin defined in class
    // out << p.getx() << " | " << p.gety();
    p.print(out);
    return out;
}

int main()
{
    cout << "3/4 == " << 3/4 << endl;
    cout << "3.0/4 == " << 3.0/4 << endl;

    Point p1;
    Point p2(3.2, 1.77);

    cout << "p1 " << p1 << endl;
    // cout << "p1 " << p1 << endl;
    // cout.operator<<(p1);
    cout << "p2 " << p2 << endl;
    cout << p1 << " + " << p2 << " = " << p1 + p2 << endl;
    cout << p1 << " + " << p2 << " = " << p1.operator+(p2) << endl;
    cout << p1 << " - " << p2 << " = " << p1 - p2 << endl;

    return 0;
}


