#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <cmath>

using namespace std;

class Graph
{
public:
    Graph(int);
    Graph(int, double, double);
    int node_count();
    int edge_count();
    double edge_value(int, int);
    void set_edge_value(int, int, double);
    size_t size();
    void dump();
private:
    int _node_count;
    int _edge_count;
    vector<double>graph_data;
};

Graph::Graph(int node_count) {

    // basic Graph constructor
    // only number of node is specified

    this->_node_count = 4;
    // this->graph_data.resize(4*4);
    graph_data.assign(4*4, 0);
}

Graph::Graph(int node_count, double edge_density, double edge_distance_max) {
    
    // Graph constructor - random generation of edges and edges distance

    double edge_distance_min = 1.0;
    default_random_engine generator(time(0));
    uniform_real_distribution<double> distribution(edge_distance_min, edge_distance_max);
    
    this->_node_count = node_count;
    // this->graph_data.resize(4*4);
    graph_data.assign(node_count*node_count, 0);
    
    // make sure the density range is between 0.1 & 1.0
    if(edge_density < 0.0)
        edge_density = 0.1;
    if(edge_density > 1.0)
        edge_density = 1.0;

    //double number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;

    int max_edge_index = _node_count * (_node_count + 1) / 2 - _node_count;
    // compute number of edges to add
    // int edge_required = _node_count * (_node_count + 1) / 2 - _node_count;
    cout << "edge required double " << static_cast<double>(max_edge_index) * edge_density << endl;
    int edge_required = round(static_cast<double>(max_edge_index) * edge_density);
    cout << "max edge index: " << max_edge_index << endl;
    cout << "edge_required: " << edge_required << endl;

    vector<int> v(max_edge_index);
    //v.assign(edge_required, 0, edge_required);

    // generate index in vector
    int current_col = 1;
    int current_row = 0;
    for(int i=0; i<v.size(); i++)
    {
        v[i] = current_col+current_row*_node_count;
        current_col++;
        if(current_col >= _node_count)
        {
            current_row++;
            current_col = current_row+1;
        }
    }

    cout << "v:" << endl;
    for(int i: v)
    {
        cout << i << endl;
    }

    std::random_device rd;
    std::mt19937 g(rd());

    std::shuffle(v.begin(), v.end(), g);

    cout << "v shuffled:" << endl;
    for(int i: v)
    {
        cout << i << endl;
    }

    for(int i=0; i<edge_required; i++)
    {
        int idx = v[i];
        int col = idx/_node_count;
        int row = idx%_node_count;
        double number = distribution(generator);
        //cout << "idx " << idx << " - col, row: " << col << " " << row << endl;
        set_edge_value(col, row, number);
    }

    //do 
    //{

    //} while(edge_added < edge_required);

}

int Graph::node_count()
{
    return _node_count;
}

int Graph::edge_count()
{
    int count;
    for(int i=0; i<graph_data.size(); i++)
        // count only if != 0 and not loop
        if(i%_node_count!=0 && graph_data[i]!=0)
            count += 1;
}

double Graph::edge_value(int i, int j)
{
    int idx = i*_node_count+j;
    return graph_data[idx];
}

void Graph::set_edge_value(int i, int j, double value)
{
    int idx1 = i*_node_count+j;
    int idx2 = j*_node_count+i;
    graph_data[idx1] = value;
    graph_data[idx2] = value;
}

void Graph::dump()
{
    // TODO: improve this with width instead of double precision
    
    // Save flags/precision.
    std::ios_base::fmtflags oldflags = std::cout.flags();
    std::streamsize oldprecision = std::cout.precision();

    std::cout << std::fixed << std::setprecision(3);
    for(int i=0; i<_node_count; i++)
    {
        for(int j=0; j<_node_count; j++)
        {
            cout << edge_value(i, j) << " ";
        }
        cout << endl;
    }

    // Restore flags/precision.
    std::cout.flags(oldflags);
    std::cout.precision(oldprecision);
    
}

size_t Graph::size()
{
    return graph_data.size();
}

int main()
{
    Graph g(4);
    cout << "node count: " << g.node_count() << endl;
    cout << "edge count: " << g.edge_count() << endl;
    // cout << "graph data size: " << g.graph_data.size() << endl;

    // test graph
    Graph g1(4);
    g1.set_edge_value(0, 1, 3);
    g1.set_edge_value(0, 2, 1);
    g1.set_edge_value(1, 3, 1);

    cout << "node count: " << g1.node_count() << endl;
    cout << "edge count: " << g1.edge_count() << endl;

    Graph g2(4, 0.5, 20.0); 
    cout << "g2 dump" << endl;
    g2.dump();
    cout << "g2 size: " << g2.size() << " - " << g2.size() * sizeof(double) / 1024.0 << "ko" << endl;

    Graph g_big(50, 0.5, 20.0); 
    cout << "g_big size: " << g_big.size() << " - " << g_big.size() * sizeof(double) / 1024.0 << "ko" << endl;
    
    // return success
    return 0;
}

