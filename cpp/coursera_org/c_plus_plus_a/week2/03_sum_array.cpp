#include <iostream>
#include <vector>

using namespace std;

template<class T>
void suma(const vector<T>&data, T& sum, T& start)
{
    sum = start;
    for(T i: data)
    {
        sum += i;
    }
}

template <class T>
ostream& operator<<(ostream &out, vector<T>&data)
{
    int j = 0;
    for(T i: data)
    {
        if(j < 5)
            out << i << " ";
        else
        {   
            out << "...";
            break;
        }
        j++;
    }
    return out;
}

int main()
{
    vector<int> d1 {3, 4, 6, 11};
    int ds1 = 0;
    int r1 = 0;
    vector<long> d2 {6, 7, 9};
    long ds2 = 24;
    long r2 = 0;
    vector<long> d3 {6, 7, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -2, -3};

    suma(d1, r1, ds1);
    suma(d2, r2, ds2);

    cout << "array d1 " << d1 << endl;
    cout << "array d3 " << d3 << endl;
    cout << "sum of d1 " << r1 << endl;
    cout << "sum of d2 " << r2 << endl;

    return 0;
}
