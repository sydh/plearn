#include <iostream>

using namespace std;

// class enum
enum class days:std::int8_t { SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY }; 

days operator++(days &day) // PREFIX OP
{
    cout << "PREFIX ";
    int _day = (static_cast<int>(day) + 1) % 7; 
    return static_cast<days>(_day);
}

days operator++(days &day, int) // POSTFIX OP
{
    cout << "POSTFIX ";
    days temp = day;
    int _day = (static_cast<int>(day) + 1) % 7;
    day = static_cast<days>(_day);
    return temp;
}

ostream& operator<<(ostream &out, days day)
{
    out << static_cast<int>(day);
    return out;
}

int main(int argc, char **argv)
{
    days today = days::MONDAY;
    days today2 {days::THURSDAY};
    cout << "today " << today << endl;
    cout << "today " << today2 << endl;
    cout << "today post incr: " << today++ << endl;
    cout << "today " << today << endl;
    cout << "today pre incr: " << ++today << endl;
    cout << "today " << today << endl;
    return 0;
}
