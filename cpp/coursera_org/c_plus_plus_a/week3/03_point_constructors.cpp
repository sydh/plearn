#include <iostream>

using namespace std;

class Point
{
public:
    
    // 2 different ways to implement constructor
    // 1: this 
    // 2: initialize list
    
    Point(int x=0.0) { this->x = static_cast<double>(x); y = 0.0; };
    Point(float x=0.0) { this->x = static_cast<double>(x); }; // this->spec = 1; };
    // initializer list is the only way to init constant
    Point(double x=0.0, double y=0.0): x(x), y(y), spec(3) {};

    void print(ostream &out) const {
        out << x << " | " << y << " |spec " << spec;
    }
private:
    double x, y;
    const int spec=2;
};

ostream &operator<<(ostream &out, const Point &p)
{
    p.print(out);
    return out;
}

int main()
{
    Point p0(1.5f);
    Point p1(1.3, 3.777);
    Point p2 {4.5, 9.9};
    Point p3(3);
    cout << p0 << endl;
    cout << p1 << endl;
    cout << p2 << endl;
    cout << p3 << endl;
    return 0;
}

