#include <iostream>

using namespace std;

class Point
{
public:
    Point(double x=0.0, double y=0.0): x(x), y(y) {};
    void print(ostream &out) const {
        out << x << " | " << y;
    }
private:
    double x, y;
};

ostream &operator<<(ostream &out, const Point &p)
{
    p.print(out);
    return out;
}

int main()
{
    Point p0;
    Point p1(1.3, 3.777);
    Point p2 {4.5, 9.9};
    cout << p0 << endl;
    cout << p1 << endl;
    cout << p2 << endl;
    return 0;
}
