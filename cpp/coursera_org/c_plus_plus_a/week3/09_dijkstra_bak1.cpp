#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <map>
#include <queue>


using namespace std;

//using edge_distance pair<int, double>;
//using edge_container vector<edge_distance>;

class Graph
{
public:
    Graph(int);
    Graph(int, double, double);
    int node_count();
    int edge_count();
    double edge_value(int, int);
    void set_edge_value(int, int, double);
    size_t size();
    void dump();
    
    void edge_to_nodes(int edge, int &node_src, int &node_dst);
    int nodes_to_edge(int node_src, int node_dst);
    int neighbours(int node, vector<int> &neighbours);
private:
    int _node_count;
    int _edge_count;
    vector<double>graph_data;
};

void Graph::edge_to_nodes(int edge, int &node_src, int &node_dst)
{
    // from an edge index, return source and destination nodes index
    // edges are defined as row -> column
    node_src = edge / _node_count;
    node_dst = edge % _node_count;
}

int Graph::nodes_to_edge(int node_src, int node_dst)
{
    // from source and dest nodes index, return edge index
    // edges are defined as row -> column
    return node_src*_node_count+node_dst;
}

int Graph::neighbours(int node, vector<int> &neighbours) 
{
    // return neighbours of given node index
    // neighbour count is returned, neighbours are in neighbours vector
    // Note: neighbours vector size >= neighbour count

    // max neighbours to the maximum of neighbours
    if(neighbours.size() < _node_count - 1)
        neighbours.resize(_node_count - 1);
    
    int nei_count = 0;

    // iterate over row
    for(int i=0; i<_node_count; i++)
    {
        if(graph_data[node*_node_count+i] > 0.0)
        {
            neighbours[nei_count] = node*_node_count+i;
            nei_count += 1;
        }
    }

    return nei_count;
}

Graph::Graph(int node_count) 
{

    // Graph constructor
    // only number of node is specified

    this->_node_count = node_count;
    // this->graph_data.resize(4*4);
    graph_data.assign(node_count*node_count, 0);
}

Graph::Graph(int node_count, double edge_density, double edge_distance_max) 
{
    // Graph constructor
    // Random generation of edges and edges distance

    double edge_distance_min = 1.0;
    default_random_engine generator(time(0));
    uniform_real_distribution<double> distribution(edge_distance_min, edge_distance_max);
    
    this->_node_count = node_count;
    // this->graph_data.resize(4*4);
    graph_data.assign(node_count*node_count, 0);
    
    // make sure the density range is between 0.1 & 1.0
    if(edge_density < 0.0)
        edge_density = 0.1;
    if(edge_density > 1.0)
        edge_density = 1.0;

    //double number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;

    int max_edge_index = _node_count * (_node_count + 1) / 2 - _node_count;
    // compute number of edges to add
    // int edge_required = _node_count * (_node_count + 1) / 2 - _node_count;
    cout << "edge required double " << static_cast<double>(max_edge_index) * edge_density << endl;
    int edge_required = round(static_cast<double>(max_edge_index) * edge_density);
    cout << "max edge index: " << max_edge_index << endl;
    cout << "edge_required: " << edge_required << endl;

    vector<int> v(max_edge_index);
    //v.assign(edge_required, 0, edge_required);

    // generate index in vector
    int current_col = 1;
    int current_row = 0;
    for(size_t i=0; i<v.size(); i++)
    {
        v[i] = current_col+current_row*_node_count;
        current_col++;
        if(current_col >= _node_count)
        {
            current_row++;
            current_col = current_row+1;
        }
    }

    cout << "v:" << endl;
    for(int i: v)
    {
        cout << i << endl;
    }

    std::random_device rd;
    std::mt19937 g(rd());

    std::shuffle(v.begin(), v.end(), g);

    cout << "v shuffled:" << endl;
    for(int i: v)
    {
        cout << i << endl;
    }

    for(int i=0; i<edge_required; i++)
    {
        int idx = v[i];
        int col = idx/_node_count;
        int row = idx%_node_count;
        double number = distribution(generator);
        //cout << "idx " << idx << " - col, row: " << col << " " << row << endl;
        set_edge_value(col, row, number);
    }

    //do 
    //{

    //} while(edge_added < edge_required);

}

int Graph::node_count()
{
    return _node_count;
}

int Graph::edge_count()
{
    /*
    int count=0;
    for(int i=0; i<graph_data.size(); i++)
    {
        cout << "i " << i << endl;
        // count only if != 0 and not loop
        if(i%_node_count!=0 && graph_data[i]!=0)
        {
            // cout << "ok i " << i << endl;
            count += 1;
        }
    }
    return count;
    */
    return _edge_count;
}

double Graph::edge_value(int node1, int node2)
{
    // int idx = i*_node_count+j;
    int edge = 
    return graph_data[idx];
}

void Graph::set_edge_value(int i, int j, double value)
{
    // TODO: rename to node1, node2 (instead of i, j)
    // TODO: could warn if i & j does not match node_count

    int idx1 = i*_node_count+j;
    int idx2 = j*_node_count+i;
    //cout << "idx1 " << idx1 << " idx2 " << idx2 << endl;
    //cout << graph_data[idx1] << " " << graph_data[idx2] << endl;
    if(graph_data[idx1] == 0 && graph_data[idx2] == 0)
        _edge_count += 1;
    graph_data[idx1] = value;
    graph_data[idx2] = value;
}

void Graph::dump()
{
    // TODO: improve this with width instead of double precision
    
    // Save flags/precision.
    std::ios_base::fmtflags oldflags = std::cout.flags();
    std::streamsize oldprecision = std::cout.precision();

    std::cout << std::fixed << std::setprecision(3);
    for(int i=0; i<_node_count; i++)
    {
        for(int j=0; j<_node_count; j++)
        {
            cout << edge_value(i, j) << " ";
        }
        cout << endl;
    }

    // Restore flags/precision.
    std::cout.flags(oldflags);
    std::cout.precision(oldprecision);
    
}

size_t Graph::size()
{
    return graph_data.size();
}


class Dijkstra
{
public:
    Dijkstra();
    Dijkstra(Graph &g): g(g) {};
    double shortest_path(const int i, const int j);
    double shortest_path(const int i, const int j, vector<int>path);
private:
    Graph &g;
};

template <typename T>
void print_map(T& m)
{
    for(auto p: m)
        cout << p.first << " - " << p.second << endl;
}

double Dijkstra::shortest_path(const int node1, const int node2)
{
    // TODO: rename to open set/close set to match course names
    
    int shortest_path_cost = 0.0;

    // closed set
    map<int, double> s1;
    
    // priority queue: open set?
    // lambda function: for sorting pair in priority_queue
    auto cmp_pair = [](pair<int, double> &p1, pair<int, double> &p2) { return p1.second > p2.second; };
    priority_queue<pair<int, double>, vector<pair<int, double>>, decltype(cmp_pair)> s2(cmp_pair);

    int node_count = g.node_count();
    
    // FIXME: start index
    // int start_index = *node_count+j;
    vector<int> nei(node_count-1);
    
    // shortest path for start index is 0
    s1.insert(make_pair(node1, 0));
    // insert adjacents

    int nei_count = g.neighbours(node1, nei); 
    
    for(int i=0; i<nei_count; i++)
    {
        s2.push(make_pair(nei[i], g.edge_value(node1, nei[i])));
    }

    cout << "Starting algo " << endl;
    cout << "s1: " << endl;
    print_map(s1);
    cout << "end s1 " << endl;
    /*
    cout << "s2: " << endl;
    while(!s2.empty())
    {
        const auto &top = s2.top();
        cout << top.first << " " << top.second << " ";
        s2.pop();
        cout << endl;
    }
    */

    //const auto &top = s2.top();
    //cout << top.first << " " << top.second << " ";
    //s2.pop();
    //const auto &top1 = s2.top();
    //cout << top1.first << " " << top1.second << " ";
    //s2.pop();

    int done = 0;
    int c = 0; // temp to prevent infinite loop
    while(!done && c < 5)
    {
        // cout << "c " << c << endl;
        const auto &cp = s2.top();
        
        // cout << "Pop pair " << cp.first << " - " << cp.second << endl;
             
        auto f = s1.find(cp.first);
        bool has_inserted = 0;
        int inserted = cp.first;
        if(f != s1.end())
        {
            // already in s1
            // only insert if it improves the cost
            if(f->second > cp.second)
            {
                 s1.insert(make_pair(cp.first, cp.second));
                 has_inserted = true;
            }
        }
        else
        {
            s1.insert(make_pair(cp.first, cp.second));
            has_inserted = true;
        }

        // remove from s2
        s2.pop();

        if(has_inserted)
        {
            cout << "has inserted " << inserted <<  endl;
            int node_src = inserted % node_count;
            cout << "node src " << node_src << endl;
            // nei_count = g.neighbours(, nei); 
            /*
            for(int i=0; i<nei_count; i++)
            {
                s2.push(make_pair(nei[i], g.edge_value(inserted, nei[i])));
            }
            */
        }

        cout << "s1 " << endl;
        print_map(s1);
        cout << "end s1 " << endl;

        c += 1;
        done = s2.empty() ? true:false;
    }

    return shortest_path_cost;

}


int main()
{
    Graph g(4);
    cout << "[g] node count: " << g.node_count() << endl;
    cout << "[g] edge count: " << g.edge_count() << endl;
    // cout << "graph data size: " << g.graph_data.size() << endl;

    // test graph
    Graph g1(4);
    g1.set_edge_value(0, 1, 3);
    g1.set_edge_value(0, 2, 1);
    g1.set_edge_value(1, 3, 1);
    g1.set_edge_value(3, 1, 2);

    cout << "[g1] node count: " << g1.node_count() << endl;
    cout << "[g1] edge count: " << g1.edge_count() << endl;

    //Graph g2(4, 0.5, 20.0); 
    //cout << "g2 dump" << endl;
    //g2.dump();
    //cout << "g2 size: " << g2.size() << " - " << g2.size() * sizeof(double) / 1024.0 << "ko" << endl;

    //Graph g_big(50, 0.5, 20.0); 
    //cout << "g_big size: " << g_big.size() << " - " << g_big.size() * sizeof(double) / 1024.0 << "ko" << endl;

    // for dijsktra algo testing

    Graph gdj1(5);
    // gdj1: 
    // nodes: A, B, C, D, E
    // A -> B: 3, B -> C: 2, C -> D: 7
    // A -> E: 5
    // indexes: A: 0, B: 1, C: 2, D: 3, E: 4
    gdj1.set_edge_value(0, 1, 3);
    gdj1.set_edge_value(1, 2, 2);
    gdj1.set_edge_value(2, 3, 7);
    gdj1.set_edge_value(0, 4, 5);
    cout << "gdj1 " << endl;
    gdj1.dump();

    int node1 = 0;
    vector<int> node1_nei(gdj1.node_count());
    int node1_nei_count = gdj1.neighbours(node1, node1_nei); 
    cout << "gdj1 node " << node1 << " has " << node1_nei_count << " neighbours" << endl;

    cout << "neighbours: " << endl;
    for(int i=0; i<node1_nei_count; i++)
    {
        cout << "nei " << node1_nei[i] << endl;
    }

    int node2 = 4;
    vector<int> node2_nei(gdj1.node_count());
    int node2_nei_count = gdj1.neighbours(node2, node2_nei); 
    cout << "gdj1 node " << node2 << " has " << node2_nei_count << " neighbours" << endl;
    
    cout << "neighbours: " << endl;
    for(int i=0; i<node2_nei_count; i++)
    {
        cout << "nei " << node2_nei[i] << endl;
    }

    int node_src = 0;
    int node_dst = 3;
    Dijkstra djk1(gdj1);
    int cost = djk1.shortest_path(node_src, node_dst);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << endl;
    cout << "======" << endl;

    // return success
    return 0;
}

