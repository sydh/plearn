#include <iostream>

using namespace std;

class Point
{
public:
    Point(double x=0.0, double y=0.0);
    ~Point();
private:
    double *coord;
};

Point::Point(double x, double y)
{
    cout << "Point constructor" << endl;
    coord = new double(2);
};

Point::~Point()
{
    cout << "Point destructor" << endl;
    delete []coord;
};

int main()
{
    int size = 4;
    char *s = new char[size];
    int *p = new int(9);
    
    s[0] = 'z';
    cout << "s first char: " << s[0] << endl;
    cout << "p adress: " << &p << endl;
    cout << "p points to value: " << *p << endl;

    delete []s;
    delete p;

    cout << "begin scope..." << endl;
    // use scope here to see P destructor
    {
        Point p;
    }
    cout << "after scope..." << endl;

    return 0;
}

