#include <iostream>
#include <queue>
#include <functional>
#include <vector>

using namespace std;

template <typename T>
void print_queue(T& q)
{
    while(!q.empty())
    {
        cout << q.top() << " ";
        q.pop();
    }
    cout << endl;
}

// TODO: how to write a generic (with template) print_queue??

/*
template <typename T>
void print_queue_pair(T& q)
{
    while(!q.empty())
    {
        T& top = q.top();
        cout << top.first << " " << top.second << " ";
        q.pop();
    }
    cout << endl;
}
*/


int main()
{
    priority_queue<int> q;
    for(int n: { 5, 2, 4, 1, 3 })
    {
        q.push(n);
    }

    print_queue(q);

    // custom compare 1
    
    //priority_queue<int, vector<int>, std::greater<int>> q0;
    priority_queue<int, vector<int>, std::less<int>> q0;
    for(int n: {31, 3, 4, 77})
    {
        q0.push(n);
    }

    print_queue(q0);

    // custom compare 2
    
    // auto cmp0 = [](int left, int right) { return right < left; };
    // priority_queue<int, decltype(cmp0)> q1(cmp0);

    /*
    for(int n: {9, 2, 3, 1, -3, 89, 99, 127, 77})
    {
        q1.push(n);
    }
    */

    // print_queue(q1);

    // now with std::pair

    auto cmp_pair = [](pair<int, double> &p1, pair<int, double> &p2) { return p1.second < p2.second; };

    pair<int, double> p1(2, 3.14);
    pair<int, double> p2(2, 3.16);
    pair<int, double> p3(2, 2.16);

    cout << cmp_pair(p1, p2) << endl;
    cout << cmp_pair(p1, p3) << endl;
    
    // TODO: what is decltype for?
    // Why do I need to declare vector<T> in priority_queue constructor?
    priority_queue<pair<int, double>, vector<pair<int, double>>, decltype(cmp_pair)> q2(cmp_pair);
    int i=0;
    for(double n: {7.8, 7.7, 3.2, 9.9, 7.7})
    {
        q2.push(make_pair(i, n));
        i++;
    }

    // print_queue_pair(q2);
    cout << "q2: " << endl;
    while(!q2.empty())
    {
        const auto &top = q2.top();
        cout << top.first << " " << top.second << " ";
        q2.pop();
        cout << endl;
    }
}

