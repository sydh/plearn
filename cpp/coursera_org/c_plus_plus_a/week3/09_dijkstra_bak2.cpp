#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <map>
#include <unordered_map>
#include <queue>
#include <cassert>


using namespace std;

//using edge_distance pair<int, double>;
//using edge_container vector<edge_distance>;

class Graph
{
public:
    Graph(int);
    Graph(int, double, double);
    int node_count();
    int edge_count();
    double edge_value(int edge); 
    double edge_value(int, int);
    void set_edge_value(int, int, double);
    size_t size();
    void dump();
    
    void edge_to_nodes(int edge, int &node_src, int &node_dst);
    int nodes_to_edge(int node_src, int node_dst);
    int neighbours(int node, vector<int> &neighbours);
private:
    int _node_count;
    int _edge_count;
    vector<double>graph_data;
};

void Graph::edge_to_nodes(int edge, int &node_src, int &node_dst)
{
    // from an edge index, return source and destination nodes index
    // edges are defined as row -> column
    node_src = edge / _node_count;
    node_dst = edge % _node_count;
}

int Graph::nodes_to_edge(int node_src, int node_dst)
{
    // from source and dest nodes index, return edge index
    // edges are defined as row -> column
    return node_src*_node_count+node_dst;
}

int Graph::neighbours(int node, vector<int> &neighbours) 
{
    // return neighbours of given node index (node dst indexes)
    // neighbour count is returned, neighbours in neighbours vector
    // Note: neighbours vector size >= neighbour count

    // set neighbours to the maximum of neighbours
    if(neighbours.size() < _node_count - 1)
        neighbours.resize(_node_count - 1);
    
    int nei_count = 0;

    // iterate over row
    for(int i=0; i<_node_count; i++)
    {
        if(graph_data[node*_node_count+i] > 0.0)
        {
            // neighbours[nei_count] = node*_node_count+i;
            neighbours[nei_count] = i;
            nei_count += 1;
        }
    }

    return nei_count;
}

Graph::Graph(int node_count) 
{
    // Graph constructor
    // only number of node is specified

    this->_node_count = node_count;
    this->_edge_count = 0;
    // this->graph_data.resize(4*4);
    graph_data.assign(node_count*node_count, 0);
}

Graph::Graph(int node_count, double edge_density, double edge_distance_max) 
{
    // Graph constructor
    // Random generation of edges and edges distance

    double edge_distance_min = 1.0;
    default_random_engine generator(time(0));
    uniform_real_distribution<double> distribution(edge_distance_min, edge_distance_max);
    
    this->_node_count = node_count;
    // this->graph_data.resize(4*4);
    graph_data.assign(node_count*node_count, 0);
    
    // make sure the density range is between 0.1 & 1.0
    if(edge_density < 0.0)
        edge_density = 0.1;
    if(edge_density > 1.0)
        edge_density = 1.0;

    //double number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;
    //number = distribution(generator);
    //cout << number << endl;

    int max_edge_index = _node_count * (_node_count + 1) / 2 - _node_count;
    // compute number of edges to add
    // int edge_required = _node_count * (_node_count + 1) / 2 - _node_count;
    //cout << "edge required double " << static_cast<double>(max_edge_index) * edge_density << endl;
    int edge_required = round(static_cast<double>(max_edge_index) * edge_density);
    //cout << "max edge index: " << max_edge_index << endl;
    //cout << "edge_required: " << edge_required << endl;

    vector<int> v(max_edge_index);
    //v.assign(edge_required, 0, edge_required);

    // generate index in vector
    int current_col = 1;
    int current_row = 0;
    for(size_t i=0; i<v.size(); i++)
    {
        v[i] = current_col+current_row*_node_count;
        current_col++;
        if(current_col >= _node_count)
        {
            current_row++;
            current_col = current_row+1;
        }
    }

    /*
    cout << "v:" << endl;
    for(int i: v)
    {
        cout << i << endl;
    }
    */

    std::random_device rd;
    std::mt19937 g(rd());

    std::shuffle(v.begin(), v.end(), g);

    /*
    cout << "v shuffled:" << endl;
    for(int i: v)
    {
        cout << i << endl;
    }
    */

    for(int i=0; i<edge_required; i++)
    {
        int idx = v[i];
        int col = idx/_node_count;
        int row = idx%_node_count;
        double number = distribution(generator);
        //cout << "idx " << idx << " - col, row: " << col << " " << row << endl;
        set_edge_value(col, row, number);
    }

    //do 
    //{

    //} while(edge_added < edge_required);

}

int Graph::node_count()
{
    return _node_count;
}

int Graph::edge_count()
{
    return _edge_count;
}

double Graph::edge_value(int edge)
{
    return graph_data[edge];
}

double Graph::edge_value(int node_src, int node_dst)
{
    // int idx = i*_node_count+j;
    int edge = nodes_to_edge(node_src, node_dst); 
    return graph_data[edge];
}

void Graph::set_edge_value(int node_src, int node_dst, double value)
{
    // TODO: rename to node1, node2 (instead of i, j)
    // TODO: could warn if i & j does not match node_count

    // int idx1 = i*_node_count+j;
    // int idx2 = j*_node_count+i;
    int edge1 = nodes_to_edge(node_src, node_dst);
    int edge2 = nodes_to_edge(node_dst, node_src);

    //cout << "idx1 " << idx1 << " idx2 " << idx2 << endl;
    //cout << graph_data[idx1] << " " << graph_data[idx2] << endl;
    if(graph_data[edge1] == 0 && graph_data[edge2] == 0)
        _edge_count += 1;
    graph_data[edge1] = value;
    graph_data[edge2] = value;
}

void Graph::dump()
{
    // TODO: improve this with width instead of double precision
    
    // Save flags/precision.
    std::ios_base::fmtflags oldflags = std::cout.flags();
    std::streamsize oldprecision = std::cout.precision();

    std::cout << std::fixed << std::setprecision(3);
    for(int i=0; i<_node_count; i++)
    {
        for(int j=0; j<_node_count; j++)
        {
            cout << edge_value(i, j) << " ";
        }
        cout << endl;
    }

    // Restore flags/precision.
    std::cout.flags(oldflags);
    std::cout.precision(oldprecision);
    
}

size_t Graph::size()
{
    return graph_data.size();
}


class Dijkstra
{
public:
    Dijkstra();
    Dijkstra(Graph &g): g(g) {};
    //double shortest_path(const int i, const int j);
    double shortest_path(const int node_src, const int node_dst, vector<int> &path);
private:
    Graph &g;
};

template <typename T>
void print_map(T& m)
{
    for(auto p: m)
        cout << p.first << " - " << p.second << endl;
}

double Dijkstra::shortest_path(const int node_src, const int node_dst, vector<int> &path)
{
    // TODO: rename to open set/close set to match course names
    
    int shortest_path_cost = -1.0;
    int node_count = g.node_count();
    // for neighbours method call
    vector<int> nei(node_count-1);

    // closed set?
    // map<edge index, total cost>
    map<int, double> s1;
    
    // priority queue: open set?
    // queue<edge_index, cost>

    // lambda function: for sorting pair in priority_queue
    auto cmp_pair = [](pair<int, double> &p1, pair<int, double> &p2) { return p1.second > p2.second; };
    priority_queue<pair<int, double>, vector<pair<int, double>>, decltype(cmp_pair)> s2(cmp_pair);

    // as we cannot iterate over elements of a priority queue
    // so we maintain a thirst structure
    // map<node_dst, cost>
    map<int, double> s3;

    //
    // initialize before algo loop    

    // shortest path for node source is 0
    int edge0 = g.nodes_to_edge(node_src, node_src);
    s1.insert(make_pair(edge0, 0));
    // insert edges of each node source neighbours
    int nei_count = g.neighbours(node_src, nei);
    for(int i=0; i<nei_count; i++)
    {
        int edge = g.nodes_to_edge(node_src, nei[i]); 
        double edge_value = g.edge_value(edge);
        s2.push(make_pair(edge, edge_value));
        s3.insert(make_pair(nei[i], edge_value));
    }

    //cout << "[0] s1:" << endl;
    //print_map(s1);
    //cout << "[0] s2 top:" << endl;
    //cout << s2.top().first << " - " << s2.top().second << endl;
    //cout << "[0] s3:" << endl;
    //print_map(s3);
    //cout << "=>" << endl;

    bool done = false;

    if(node_src == node_dst)
    {
        // early exit for special case
        shortest_path_cost = 0.0;
        done = true;
    }

    int c = 0; // temp to prevent infinite loop
    while(!done && c < 50)
    {
        // pop edge with min cost - best cost
        const auto &edge_dist = s2.top();
        // insert into s1
        s1.insert(make_pair(edge_dist.first, edge_dist.second));

        //cout << "s1:" << endl;
        //print_map(s1);

        int edge = edge_dist.first;
        double _cost = edge_dist.second;
        //cout << "current edge " << edge << " - cost " << edge_dist.second << endl;
        // remove this
        s2.pop();
        // with this added edge, retrieve node_dst &&
        
        int _node_src;
        int _node_dst;
        //cout << "current edge " << edge << " - cost " << edge_dist.second << endl;
        g.edge_to_nodes(edge, _node_src, _node_dst);

        if(_node_dst == node_dst)
        {
            // early exit - node dst is reached...
            shortest_path_cost = _cost;
            break;
        }
        
        int nei_count = g.neighbours(_node_dst, nei);
        //cout << "node " << _node_dst << " - nei count: " << nei_count << endl;

        //cout << "current edge " << edge << " - cost " << edge_dist.second << endl;
        for(int i=0; i<nei_count; i++)
        {
            //cout << "node " << _node_dst << " - nei: " << nei[i] << endl;

            int edge1 = g.nodes_to_edge(_node_dst, nei[i]);
            int edge2 = g.nodes_to_edge(nei[i], _node_dst);
            double edge_value = g.edge_value(edge1);

            //cout << "current node src " << _node_dst << endl;
            //cout << "Checking edges " << edge1 << " - " << edge2 << endl;

            // before inserting with (edge, edge value) into 
            // s2, need to check
            // edge1 or edge2 is not already in s1
            // cost to reach nei[i] is not higher than cost in s3

            auto edge1_f = s1.find(edge1);
            bool edge1_found = (edge1_f != s1.end());
            if(edge1_found)
                continue;
            
            auto edge2_f = s1.find(edge2);
            bool edge2_found = (edge2_f != s1.end()); 
            if(edge2_found)
                continue;

            auto nei_f = s3.find(nei[i]);
            bool nei_found = (nei_f != s3.end());
            if(nei_found && _cost+edge_value >= nei_f->second)
                continue;

            // all good, insert into s2
            //cout << "_cost " << _cost << " - edge_value: " << edge_value << endl;
            //cout << "add to s2, edge: " << edge1 << " - cost: " << _cost+edge_value << endl;
            s2.push(make_pair(edge1, _cost+edge_value));
            s3.insert(make_pair(nei[i], _cost+edge_value));
            
            /*
            if(edge1_found)
                cost = edge1_f->second;
            else if(edge2_found)
                cost = edge2_f->second;
            else
                continue;

            auto nei_f = s3.find(edge_value);
            double node_dst_cost = cost + edge_value; 
            if(node_dst_cost >= nei_f->second)
                continue;

            // all good, insert into s2
            s2.push(make_pair(edge1, node_dst_cost));
            s3.insert(make_pair(nei[i], node_dst_cost));
            */

            //cout << "=====" << endl;
        }

        done = s2.empty();
    }

    if(shortest_path_cost >= 0.0)
    {
        //cout << "found shortest path..." << endl;
        //print_map(s1);
        //cout << "end s1" << endl;

        // use a temp new map to store key = s1.value, value = s1.key
        multimap<int, double> s1r;
        for(auto p: s1)
            s1r.insert(make_pair(p.second, p.first));

        //print_map(s1r);
        //cout << "===" << endl;

        // find path to go from node_src to node_dst
        path[0] = node_dst;
        int idx = 1;
        // iterate over s1r in reverse order
        for(auto iter=s1r.rbegin(); iter!=s1r.rend(); ++iter)
        {
            int edge = iter->second;
            int _node_src, _node_dst;
            g.edge_to_nodes(edge, _node_src, _node_dst);
            //cout << "edge " << edge << " - " << _node_src << " - " << _node_dst << endl;
            if(_node_dst == path[idx-1])
            {
                path[idx] = _node_src;
                idx++;
            }
        }
    }
    
    return shortest_path_cost;

}

void tests()
{
    // test Graph functions with assert

    Graph g(4);
    cout << "[g] node count: " << g.node_count() << endl;
    cout << "[g] edge count: " << g.edge_count() << endl;
    assert(g.node_count() == 4);
    assert(g.edge_count() == 0);

    Graph g1(4);
    g1.set_edge_value(0, 1, 3);
    g1.set_edge_value(0, 2, 1);
    g1.set_edge_value(1, 3, 1);
    g1.set_edge_value(3, 1, 2);

    cout << "[g0] node count: " << g1.node_count() << endl;
    cout << "[g1] edge count: " << g1.edge_count() << endl;

    assert(g1.node_count() == 4); 
    assert(g1.edge_count() == 3); // 1 edge is set twice

    Graph g2(4, 0.5, 20.0); 
    cout << "g2 dump" << endl;
    g2.dump();
    cout << "g2 size: " << g2.size() << " - " << g2.size() * sizeof(double) / 1024.0 << "ko" << endl;

    assert(g2.node_count() == 4);

    Graph g_big(50, 0.5, 20.0); 
    cout << "g_big size: " << g_big.size() << " - " << g_big.size() * sizeof(double) / 1024.0 << "ko" << endl;

    assert(g2.node_count() == 4);

    Graph gdj1(3);
    // gdj1: 
    // nodes: A, B, C
    // A -> B: 3, B -> C: 4, A -> C: 7
    // indexes: A: 0, B: 1, C: 2
    gdj1.set_edge_value(0, 1, 3);
    gdj1.set_edge_value(1, 2, 4);
    gdj1.set_edge_value(0, 2, 7);
    cout << "gdj1 " << endl;
    gdj1.dump();

    assert(gdj1.node_count() == 3);
    assert(gdj1.edge_count() == 3);
    
    vector<int> path(gdj1.node_count());
    Dijkstra djk1(gdj1);
    int node_src = 0; int node_dst = 1;
    double cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==3);
    assert(path[0]==1);
    assert(path[1]==0);

    node_src = 1; node_dst = 2;
    cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==4);
    assert(path[0]==2);
    assert(path[1]==1);

    node_src = 0; node_dst = 2;
    cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==7);
    // path will be 0 -> 2 and not 0, 1, 2
    assert(path[0]==2 && path[1] == 0);

    node_src = 2; node_dst = 2;
    cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==0);
    // path will be 0 -> 2 and not 0, 1, 2
    assert(path[0]==2);
}

int main()
{
    //Graph g(4);
    //cout << "[g] node count: " << g.node_count() << endl;
    //cout << "[g] edge count: " << g.edge_count() << endl;
    // cout << "graph data size: " << g.graph_data.size() << endl;

    // test graph
    //Graph g1(4);
    //g1.set_edge_value(0, 1, 3);
    //g1.set_edge_value(0, 2, 1);
    //g1.set_edge_value(1, 3, 1);
    //g1.set_edge_value(3, 1, 2);

    //cout << "[g0] node count: " << g1.node_count() << endl;
    //cout << "[g1] edge count: " << g1.edge_count() << endl;

    //Graph g2(4, 0.5, 20.0); 
    //cout << "g2 dump" << endl;
    //g2.dump();
    //cout << "g2 size: " << g2.size() << " - " << g2.size() * sizeof(double) / 1024.0 << "ko" << endl;

    //Graph g_big(50, 0.5, 20.0); 
    //cout << "g_big size: " << g_big.size() << " - " << g_big.size() * sizeof(double) / 1024.0 << "ko" << endl;

    tests();

    // for dijsktra algo testing

    Graph gdj1(5);
    // gdj1: 
    // nodes: A, B, C, D, E
    // A -> B: 3, B -> C: 2, C -> D: 7
    // A -> E: 5
    // indexes: A: 0, B: 1, C: 2, D: 3, E: 4
    gdj1.set_edge_value(0, 1, 3);
    gdj1.set_edge_value(1, 2, 2);
    gdj1.set_edge_value(2, 3, 7);
    gdj1.set_edge_value(0, 4, 5);
    cout << "gdj1 " << endl;
    gdj1.dump();
    
    /*
    int node1 = 0;
    vector<int> node1_nei(gdj1.node_count());
    int node1_nei_count = gdj1.neighbours(node1, node1_nei); 
    cout << "gdj1 node " << node1 << " has " << node1_nei_count << " neighbours" << endl;

    cout << "neighbours: " << endl;
    for(int i=0; i<node1_nei_count; i++)
    {
        cout << "nei " << node1_nei[i] << endl;
    }

    int node2 = 4;
    vector<int> node2_nei(gdj1.node_count());
    int node2_nei_count = gdj1.neighbours(node2, node2_nei); 
    cout << "gdj1 node " << node2 << " has " << node2_nei_count << " neighbours" << endl;
    
    cout << "neighbours: " << endl;
    for(int i=0; i<node2_nei_count; i++)
    {
        cout << "nei " << node2_nei[i] << endl;
    }
    */

    int node_src = 0;
    int node_dst = 3;
    vector<int> path(gdj1.node_count());
    int node_count = -1;
    Dijkstra djk1(gdj1);
    int cost = djk1.shortest_path(node_src, node_dst, path);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "path (reverse order): " << endl;
    for(int i: path)
    {
        cout << "node " << i << endl;
        if(i==node_src)
            break;
    }
    cout << "======" << endl;

    /*
    node_src = 3;
    node_dst = 0;
    cost = djk1.shortest_path(node_src, node_dst);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "======" << endl;
    */

    node_src = 4;
    node_dst = 4;
    cost = djk1.shortest_path(node_src, node_dst, path);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "path (reverse order): " << endl;
    for(int i: path)
    {
        cout << "node " << i << endl;
        if(i==node_src)
            break;
    }
    cout << "======" << endl;
    cout << "======" << endl;

    node_src = 4;
    node_dst = 3;
    cost = djk1.shortest_path(node_src, node_dst, path);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "======" << endl;

    cout << "path (reverse order): " << endl;
    for(int i: path)
    {
        cout << "node " << i << endl;
        if(i==node_src)
            break;
    }
    cout << "======" << endl;

    // return success
    return 0;
}

