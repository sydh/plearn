#include <iostream>
#include <map>

using namespace std;

template <typename T>
void print_map(T& m)
{
    for(auto p: m)
        cout << p.first << " - " << p.second << endl;
}

int main()
{
    std::map<int, double> m1;
    cout << "m1: " << endl;
    print_map(m1);
    m1.insert(make_pair(7, 3.14));
    m1.insert(make_pair(4, 7.14));
    m1.insert(make_pair(1, 0.14));
    cout << "m1: " << endl;
    print_map(m1);

    // find in map
    cout << "find key 4 in map:" << endl;
    auto f4 = m1.find(4);
    if(f4 != m1.end())
    {
        cout << f4->first << " | " << f4->second << endl;
    }

}
