// coursera 
// week3 
// implement shortest path algo (dijkstra)
//
// Compile with:
// clang++ -Wall -Wextra 09_dijkstra.cpp -o 09_dijkstra
// or with:
// g++ -Wall -Wextra 09_dijkstra.cpp -o 09_dijkstra

// run:
// ./09_dijkstra

// from week2:  
// I've implemented a Graph class using vector (flat array)
// Advantages:
// 1- No dynamic allocation (only 1 vector)
// 2- Easy acces to edge value && easy unique indexes for edges
// 3- Easy to write function to convert from edge index to nodes (and vice versa)
// Drawbacks: 
// 1- Consume more memory than needed (~twice more for unidirectional graph)
// 2- Some tests need to be done twice as index of edge 0 -> 1 != index of edge 1 -> 0
// 
// What I've learned: 
// C++ allow some good data container abstraction
// Use of stl data structure: map, priority queue, vector
// Use of others things: lambda function, iterator

#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <map>
// #include <unordered_map>
#include <queue>
#include <cassert>


using namespace std;

class Graph
{
public:
    Graph(int);
    Graph(int, double, double);
    int node_count();
    int edge_count();
    double edge_value(int); 
    double edge_value(int, int);
    void set_edge_value(int, int, double);
    size_t size();
    void dump();
    
    void edge_to_nodes(int edge, int &node_src, int &node_dst);
    int nodes_to_edge(int node_src, int node_dst);
    int neighbours(int node, vector<int> &neighbours);
private:
    int _node_count;
    int _edge_count;
    vector<double>graph_data;
};

void Graph::edge_to_nodes(int edge, int &node_src, int &node_dst)
{
    // from an edge index, return source and destination nodes index
    // edges are defined as row -> column
    node_src = edge / _node_count;
    node_dst = edge % _node_count;
}

int Graph::nodes_to_edge(int node_src, int node_dst)
{
    // from source and dest nodes index, return edge index
    // edges are defined as row -> column
    return node_src*_node_count+node_dst;
}

int Graph::neighbours(int node, vector<int> &neighbours) 
{
    // return neighbours of given node index (node dst indexes)
    // neighbour count is returned, neighbours in neighbours vector
    // Note: neighbours vector size >= neighbour count
    // Note: vector neighbours can be resized if too small

    // set neighbours to the maximum of neighbours
    if(neighbours.size() < static_cast<size_t>(_node_count - 1))
        neighbours.resize(_node_count - 1);
    
    int nei_count = 0;

    // iterate over row
    for(int i=0; i<_node_count; i++)
    {
        if(graph_data[node*_node_count+i] > 0.0)
        {
            // neighbours[nei_count] = node*_node_count+i;
            neighbours[nei_count] = i;
            nei_count += 1;
        }
    }

    return nei_count;
}

Graph::Graph(int node_count) 
{
    // Graph constructor
    // only number of node is specified

    this->_node_count = node_count;
    this->_edge_count = 0;
    // this->graph_data.resize(4*4);
    graph_data.assign(node_count*node_count, 0);
}

Graph::Graph(int node_count, double edge_density, double edge_distance_max) 
{
    // Graph constructor
    // Random generation of edges and edges distance

    double edge_distance_min = 1.0;
    default_random_engine generator(time(0));
    uniform_real_distribution<double> distribution(edge_distance_min, edge_distance_max);
    
    this->_node_count = node_count;
    // this->graph_data.resize(4*4);
    graph_data.assign(node_count*node_count, 0);
    
    // make sure the density range is between 0.1 & 1.0
    if(edge_density < 0.0)
        edge_density = 0.1;
    if(edge_density > 1.0)
        edge_density = 1.0;

    // number of edges (for unidirectional graph) without edge loop
    int max_edge_index = _node_count * (_node_count + 1) / 2 - _node_count;
    int edge_required = round(static_cast<double>(max_edge_index) * edge_density);
    vector<int> v(max_edge_index);

    // set edge index in vector
    int current_col = 1;
    int current_row = 0;
    for(size_t i=0; i<v.size(); i++)
    {
        v[i] = current_col+current_row*_node_count;
        current_col++;
        if(current_col >= _node_count)
        {
            current_row++;
            current_col = current_row+1;
        }
    }

    // shuffle vector 
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(v.begin(), v.end(), g);

    // Take number of required edges from random indexes
    // and generate random value

    for(int i=0; i<edge_required; i++)
    {
        int idx = v[i];
        int col = idx/_node_count;
        int row = idx%_node_count;
        double number = distribution(generator);
        set_edge_value(col, row, number);
    }

}

int Graph::node_count()
{
    // get number of nodes in graph
    return _node_count;
}

int Graph::edge_count()
{
    // get number of edges in graph
    return _edge_count;
}

double Graph::edge_value(int edge)
{
    // get cost associated to edge index
    return graph_data[edge];
}

double Graph::edge_value(int node_src, int node_dst)
{
    // get cost associated to edge going from src to dst nodes
    int edge = nodes_to_edge(node_src, node_dst); 
    return graph_data[edge];
}

void Graph::set_edge_value(int node_src, int node_dst, double value)
{
    // Set edge value (eg. cost) to go from src to dest nodes
    // TODO: could warn if node_src & node_dst are out of bounds for current graph

    // unidirectional graph so same value for both direction
    int edge1 = nodes_to_edge(node_src, node_dst);
    int edge2 = nodes_to_edge(node_dst, node_src);

    if(graph_data[edge1] == 0 && graph_data[edge2] == 0)
        _edge_count += 1;

    graph_data[edge1] = value;
    graph_data[edge2] = value;
}

void Graph::dump()
{
    // TODO: improve this with width instead of double precision
    
    // Save flags/precision.
    std::ios_base::fmtflags oldflags = std::cout.flags();
    std::streamsize oldprecision = std::cout.precision();

    std::cout << std::fixed << std::setprecision(3);
    for(int i=0; i<_node_count; i++)
    {
        for(int j=0; j<_node_count; j++)
        {
            cout << edge_value(i, j) << " ";
        }
        cout << endl;
    }

    // Restore flags/precision.
    std::cout.flags(oldflags);
    std::cout.precision(oldprecision);
    
}

size_t Graph::size()
{
    // get size of graph (eg. underlying container size)
    // multiply this value with sizeof(double) to get the size in bytes 
    return graph_data.size();
}


class Dijkstra
{
public:
    Dijkstra();
    Dijkstra(Graph &g): g(g) {};
    double shortest_path(const int node_src, const int node_dst, vector<int> &path);
private:
    Graph &g;
};

template <typename T>
void print_map(T& m)
{
    // debug function to print std::map content
    for(auto p: m)
        cout << p.first << " - " << p.second << endl;
}

double Dijkstra::shortest_path(const int node_src, const int node_dst, vector<int> &path)
{
    // return shortest path from src to dst node
    // return shortest path cost as double and node indexes in path 
    // note: node indexes in path are in reverse order (node dst -> node src)

    int shortest_path_cost = -1.0;
    int node_count = g.node_count();
    // for neighbours method call
    vector<int> nei(node_count-1);

    // closed set?
    // map<edge index, total cost>
    map<int, double> s1;
    
    // priority queue: open set?
    // queue<edge_index, cost>

    // lambda function: for sorting pair in priority_queue
    auto cmp_pair = [](pair<int, double> &p1, pair<int, double> &p2) { return p1.second > p2.second; };
    priority_queue<pair<int, double>, vector<pair<int, double>>, decltype(cmp_pair)> s2(cmp_pair);

    // as we cannot iterate over elements of a priority queue
    // so we maintain a third structure: map<node_dst, cost> 
    map<int, double> s3;

    //
    // initialize before algo loop    

    // shortest path for node source is 0
    int edge0 = g.nodes_to_edge(node_src, node_src);
    s1.insert(make_pair(edge0, 0));
    // insert edges of each node source neighbours
    int nei_count = g.neighbours(node_src, nei);
    for(int i=0; i<nei_count; i++)
    {
        int edge = g.nodes_to_edge(node_src, nei[i]); 
        double edge_value = g.edge_value(edge);
        s2.push(make_pair(edge, edge_value));
        s3.insert(make_pair(nei[i], edge_value));
    }

    bool done = false;
    if(node_src == node_dst)
    {
        // early exit for special case
        shortest_path_cost = 0.0;
        done = true;
    }

    int c = 0; // temp to prevent infinite loop
    while(!done && c < 50)
    {
        // pop edge with min cost - best cost
        const auto &edge_dist = s2.top();
        // insert into s1
        s1.insert(make_pair(edge_dist.first, edge_dist.second));

        int edge = edge_dist.first;
        double _cost = edge_dist.second;
        // now remove element 
        s2.pop();
        
        int _node_src;
        int _node_dst;
        g.edge_to_nodes(edge, _node_src, _node_dst);

        if(_node_dst == node_dst)
        {
            // early exit - node dst is reached...
            shortest_path_cost = _cost;
            break;
        }
        
        // Now append neighbours of current _node_dst
        int nei_count = g.neighbours(_node_dst, nei);
        for(int i=0; i<nei_count; i++)
        {
            int edge1 = g.nodes_to_edge(_node_dst, nei[i]);
            int edge2 = g.nodes_to_edge(nei[i], _node_dst);
            double edge_value = g.edge_value(edge1);

            auto edge1_f = s1.find(edge1);
            bool edge1_found = (edge1_f != s1.end());
            // edge already in s1 (best cost already found)
            if(edge1_found)
                continue;
            
            auto edge2_f = s1.find(edge2);
            bool edge2_found = (edge2_f != s1.end()); 
            // edge already in s1 (best cost already found)
            if(edge2_found)
                continue;

            auto nei_f = s3.find(nei[i]);
            bool nei_found = (nei_f != s3.end());
            // already a path to go to this neighbour or cost is not improved
            if(nei_found && _cost+edge_value >= nei_f->second)
                continue;

            // all good, insert into s2 (&& s3)
            s2.push(make_pair(edge1, _cost+edge_value));
            s3.insert(make_pair(nei[i], _cost+edge_value));
        }

        done = s2.empty(); // exit in case all pathes in graph have been found
    }

    if(shortest_path_cost >= 0.0)
    {
        // use a temp new map to store key = s1.value, value = s1.key
        multimap<int, double> s1r;
        for(auto p: s1)
            s1r.insert(make_pair(p.second, p.first));

        // find path to go from node_src to node_dst
        path[0] = node_dst;
        int idx = 1;
        // iterate over s1r in reverse order
        for(auto iter=s1r.rbegin(); iter!=s1r.rend(); ++iter)
        {
            int edge = iter->second;
            int _node_src, _node_dst;
            g.edge_to_nodes(edge, _node_src, _node_dst);
            if(_node_dst == path[idx-1])
            {
                path[idx] = _node_src;
                idx++;
            }
        }
    }
    
    return shortest_path_cost;

}

void tests()
{
    // test Graph && Dijkstra functions with assert
    // Note: keep it here so easy submissions but should be in separate file (unit tests)

    // test: simplest Graph
    Graph g(4);
    cout << "[g] node count: " << g.node_count() << endl;
    cout << "[g] edge count: " << g.edge_count() << endl;
    assert(g.node_count() == 4);
    assert(g.edge_count() == 0);

    // test: Graph + edges
    Graph g1(4);
    g1.set_edge_value(0, 1, 3);
    g1.set_edge_value(0, 2, 1);
    g1.set_edge_value(1, 3, 1);
    g1.set_edge_value(3, 1, 2);

    cout << "[g0] node count: " << g1.node_count() << endl;
    cout << "[g1] edge count: " << g1.edge_count() << endl;

    assert(g1.node_count() == 4); 
    assert(g1.edge_count() == 3); // 1 edge (1->3 && 3->1) is set twice
    
    // test: Graph edge value function(s)
    assert(g1.edge_value(0) == 0); // edge from node 0 -> 0
    assert(g1.edge_value(1) == 3); // edge from node 0 -> 1
    assert(g1.edge_value(3, 1) == 2);

    // test: Graph from edge_density && random edge values
    Graph g2(4, 0.5, 20.0); 
    cout << "g2 dump" << endl;
    g2.dump();
    cout << "g2 size: " << g2.size() << " - " << g2.size() * sizeof(double) / 1024.0 << "ko" << endl;

    assert(g2.node_count() == 4);

    Graph g_big(50, 0.5, 20.0); 
    cout << "g_big size: " << g_big.size() << " - " << g_big.size() * sizeof(double) / 1024.0 << "ko" << endl;

    assert(g2.node_count() == 4);

    // test: Graph + edges & Dijkstra
    // Note: all edges are connected

    Graph gdj1(3);
    // gdj1: 
    // nodes: A, B, C
    // A -> B: 3, B -> C: 4, A -> C: 7
    // indexes: A: 0, B: 1, C: 2
    gdj1.set_edge_value(0, 1, 3);
    gdj1.set_edge_value(1, 2, 4);
    gdj1.set_edge_value(0, 2, 7);
    cout << "gdj1 " << endl;
    gdj1.dump();

    assert(gdj1.node_count() == 3);
    assert(gdj1.edge_count() == 3);
    
    // test: neighbours function
    vector<int> nei(gdj1.node_count());
    int nei_count = gdj1.neighbours(0, nei); 
    assert(nei_count==2);
    assert(nei[0]==1);
    assert(nei[1]==2);

    // tests: shortest path function
    vector<int> path(gdj1.node_count());
    Dijkstra djk1(gdj1);
    int node_src = 0; int node_dst = 1;
    double cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==3);
    assert(path[0]==1);
    assert(path[1]==0);

    node_src = 1; node_dst = 2;
    cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==4);
    assert(path[0]==2);
    assert(path[1]==1);

    node_src = 0; node_dst = 2;
    cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==7);
    // path will be 0 -> 2 and not 0, 1, 2
    assert(path[0]==2 && path[1] == 0);
    
    // test: same src and dst nodes
    node_src = 2; node_dst = 2;
    cost = djk1.shortest_path(node_src, node_dst, path);
    assert(static_cast<int>(cost)==0);
    assert(path[0]==2);

    // test: Graph + edges & Dijkstra
    // Note: not all edges are connected
    
    Graph gdj2(3);
    // gdj1: 
    // nodes: A, B, C
    // A -> B: 99
    // indexes: A: 0, B: 1, C: 2
    assert(gdj2.edge_count() == 0);
    gdj2.set_edge_value(0, 1, 99);
    cout << "gdj2 " << endl;
    gdj2.dump();

    assert(gdj2.node_count() == 3); 
    // cout << gdj2.edge_count() << endl;
    assert(gdj2.edge_count() == 1);
    
    // test: shortest path function (connected edge)
    vector<int> path2(gdj2.node_count());
    Dijkstra djk2(gdj2);
    node_src = 0; node_dst = 1;
    cost = djk2.shortest_path(node_src, node_dst, path2);
    assert(static_cast<int>(cost)==99);
    assert(path2[0]==1);
    assert(path2[1]==0);

    node_src = 1; node_dst = 0;
    cost = djk2.shortest_path(node_src, node_dst, path2);
    assert(static_cast<int>(cost)==99);
    assert(path2[0]==0);
    assert(path2[1]==1);

    node_src = 0; node_dst = 2;
    cost = djk2.shortest_path(node_src, node_dst, path2);
    assert(static_cast<int>(cost)<0.0);

    node_src = 1; node_dst = 2;
    cost = djk2.shortest_path(node_src, node_dst, path2);
    assert(static_cast<int>(cost)<0.0);
}


int main()
{
    cout << "Running unit tests..." << endl;
    tests();    

    cout << "Now running simulation..." << endl;

    // g20: 50 nodes, 20% densities from 1.0 to 10.0
    Graph g20(50, 0.2, 10.0); 
    
    // compute costs for 49 paths: 1->2 ... 1->50
    double cost_sum = 0.0;
    int cost_found = 0;
    int node_src = 0;
    vector<int> path(g20.node_count());
    Dijkstra dj_g20(g20);

    for(int node_dst=1; node_dst<g20.node_count(); node_dst++)
    {
        double cost = dj_g20.shortest_path(node_src, node_dst, path);
        // if cost < 0, could not found path 
        if(cost >= 0.0)
        {
            cost_sum += cost;
            cost_found++;
        }
    }

    // average cost
    double average = cost_sum/static_cast<double>(cost_found);
    cout << "Average cost for graph g20 " << average << endl;
    cout << "Number of path found " << cost_found << endl;

    // g50: 50 nodes, 20% densities from 1.0 to 10.0
    Graph g50(50, 0.4, 10.0); 

    // compute costs for 49 paths: 1->2 ... 1->50

    cost_sum = 0.0;
    cost_found = 0;
    path.resize(g50.node_count());
    Dijkstra dj_g50(g50);

    for(int node_dst=1; node_dst<g50.node_count(); node_dst++)
    {
        double cost = dj_g50.shortest_path(node_src, node_dst, path);
        // if cost < 0, could not found path 
        if(cost >= 0.0)
        {
            cost_sum += cost;
            cost_found++;
        }
    }
    
    // average cost
    average = cost_sum/static_cast<double>(cost_found);
    cout << "Average cost for graph g50 " << average << endl;
    cout << "Number of path found " << cost_found << endl;
}

int main2()
{
    //Graph g(4);
    //cout << "[g] node count: " << g.node_count() << endl;
    //cout << "[g] edge count: " << g.edge_count() << endl;
    // cout << "graph data size: " << g.graph_data.size() << endl;

    // test graph
    //Graph g1(4);
    //g1.set_edge_value(0, 1, 3);
    //g1.set_edge_value(0, 2, 1);
    //g1.set_edge_value(1, 3, 1);
    //g1.set_edge_value(3, 1, 2);

    //cout << "[g0] node count: " << g1.node_count() << endl;
    //cout << "[g1] edge count: " << g1.edge_count() << endl;

    //Graph g2(4, 0.5, 20.0); 
    //cout << "g2 dump" << endl;
    //g2.dump();
    //cout << "g2 size: " << g2.size() << " - " << g2.size() * sizeof(double) / 1024.0 << "ko" << endl;

    //Graph g_big(50, 0.5, 20.0); 
    //cout << "g_big size: " << g_big.size() << " - " << g_big.size() * sizeof(double) / 1024.0 << "ko" << endl;

    tests();

    // for dijsktra algo testing

    Graph gdj1(5);
    // gdj1: 
    // nodes: A, B, C, D, E
    // A -> B: 3, B -> C: 2, C -> D: 7
    // A -> E: 5
    // indexes: A: 0, B: 1, C: 2, D: 3, E: 4
    gdj1.set_edge_value(0, 1, 3);
    gdj1.set_edge_value(1, 2, 2);
    gdj1.set_edge_value(2, 3, 7);
    gdj1.set_edge_value(0, 4, 5);
    cout << "gdj1 " << endl;
    gdj1.dump();
    
    /*
    int node1 = 0;
    vector<int> node1_nei(gdj1.node_count());
    int node1_nei_count = gdj1.neighbours(node1, node1_nei); 
    cout << "gdj1 node " << node1 << " has " << node1_nei_count << " neighbours" << endl;

    cout << "neighbours: " << endl;
    for(int i=0; i<node1_nei_count; i++)
    {
        cout << "nei " << node1_nei[i] << endl;
    }

    int node2 = 4;
    vector<int> node2_nei(gdj1.node_count());
    int node2_nei_count = gdj1.neighbours(node2, node2_nei); 
    cout << "gdj1 node " << node2 << " has " << node2_nei_count << " neighbours" << endl;
    
    cout << "neighbours: " << endl;
    for(int i=0; i<node2_nei_count; i++)
    {
        cout << "nei " << node2_nei[i] << endl;
    }
    */

    int node_src = 0;
    int node_dst = 3;
    vector<int> path(gdj1.node_count());
    // int node_count = -1;
    Dijkstra djk1(gdj1);
    int cost = djk1.shortest_path(node_src, node_dst, path);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "path (reverse order): " << endl;
    for(int i: path)
    {
        cout << "node " << i << endl;
        if(i==node_src)
            break;
    }
    cout << "======" << endl;

    /*
    node_src = 3;
    node_dst = 0;
    cost = djk1.shortest_path(node_src, node_dst);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "======" << endl;
    */

    node_src = 4;
    node_dst = 4;
    cost = djk1.shortest_path(node_src, node_dst, path);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "path (reverse order): " << endl;
    for(int i: path)
    {
        cout << "node " << i << endl;
        if(i==node_src)
            break;
    }
    cout << "======" << endl;
    cout << "======" << endl;

    node_src = 4;
    node_dst = 3;
    cost = djk1.shortest_path(node_src, node_dst, path);
    cout << "Shortest path from node " << node_src << " to " << node_dst << endl;
    cout << "cost: " << cost << endl;
    cout << "======" << endl;

    cout << "path (reverse order): " << endl;
    for(int i: path)
    {
        cout << "node " << i << endl;
        if(i==node_src)
            break;
    }
    cout << "======" << endl;

    // return success
    return 0;
}

