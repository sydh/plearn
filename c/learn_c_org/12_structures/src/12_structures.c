#include <stdio.h>
#include <assert.h>

// basic struct declaration
struct point {
    int x;
    int y;
};

// using typedef with struct
typedef struct {
    char* brand;
    int model;
} vehicle;

int main() {

    struct point p1;
    p1.x = 1; p1.y = 2;
    printf("point p1: %d - %d\n", p1.x, p1.y);
    struct point p2 = { .x =  3, .y =  5};
    struct point p3 = {3, 5};
    printf("point p2: %d - %d\n", p2.x, p2.y);
    printf("point p3: %d - %d\n", p3.x, p3.y);

    vehicle v1;
    v1.brand = "FORD";
    v1.model = 2007;
    assert(v1.model == 2007);
    vehicle v2 = {.model = 1};
    assert(v2.brand == NULL);
    assert(v2.model == 1);

    return 0;
}
