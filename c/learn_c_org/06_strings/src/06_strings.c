#include <stdio.h>
#include <string.h>

int main() {
    char *s1 = "hello world!";
    char s2[] = "hello John!";
    char s3[12] = "hello John!";
    // Warning: not enough space alloc
    // Use flag: -Wc++-compat for gcc to detect it
    char s4[11] = "hello John!";
    printf("string s1: %s\n", s1);
    printf("string s2: %s\n", s2);
    printf("string s3: %s\n", s3);
    printf("string s4: %s\n", s4);

    // length of a string
    size_t len1 = strlen(s1);
    // Note that %zu formatting requires C99
    printf("len of string s1 (\"%s\"): %zu\n", s1, len1);

    // string comparison
    // Note: use strncmp instead of strcmp (safer as the max len comparison)
    if (strncmp(s1, s2, len1) == 0) {
        printf("s1 == s2\n");
    } else {
        printf("s1 != s2\n");
    }

    // string concat
    char dest[20]="Hello";
    char src[20]="World";
    strncat(dest, src, 3);
    printf("%s\n",dest);
    strncat(dest, src, 20);
    printf("%s\n",dest);

    return 0;
}