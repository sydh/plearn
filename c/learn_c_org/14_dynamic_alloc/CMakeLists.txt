cmake_minimum_required(VERSION 3.13)
project(14_alloc)
add_executable(
        14_alloc
        src/14_alloc.c
)

target_compile_options(14_alloc PRIVATE
        $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:
        -Wall -Wextra -pedantic -Werror>
        $<$<CXX_COMPILER_ID:MSVC>:
        /W4>)