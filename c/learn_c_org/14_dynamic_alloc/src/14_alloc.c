#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


typedef struct {
    char *name;
    int age;
} person_t;

int main() {

    person_t* p1 = (person_t* )malloc(sizeof(person_t)*1);
    p1->name = "JOE";
    p1->age = 85;

    free(p1);
    // Not mandatory but prevent p1 address to be reused later one
    p1 = NULL;

    int16_t *array_int16s = (int16_t *)malloc(sizeof(int16_t)*2);
    array_int16s[0] = 9;
    array_int16s[1] = 8;
    // Uncomment this to see out of bounds access with Valgrind
    // array_int16s[2] = 7;

    // Comment this and run with valgrind:
    // /usr/bin/valgrind --tool=memcheck --xml=yes --xml-file=/tmp/valgrind --gen-suppressions=all --leak-check=full --leak-resolution=med --track-origins=yes --vgdb=no /home/sydh/dev/plearn/c/learn_c_org/cmake-build-debug/14_dynamic_alloc/14_alloc
    // to see memory leak
    free(array_int16s);
    array_int16s = NULL;

    // multid dynamic array
    char **hello_names = (char **)malloc(sizeof(char *)*2);
    hello_names[0] = (char *)malloc(sizeof(char)*4); // +1 char for \0
    hello_names[1] = (char *)malloc(sizeof(char)*6);
    strncpy(hello_names[0], "joe", 4);
    strncpy(hello_names[1], "sonia", 6);
    for(int i=0; i<2; i++) {
        printf("Hello %s\n", hello_names[i]);
    }
    printf("===");
    free(hello_names[0]);
    free(hello_names[1]);
    free(hello_names);

    return 0;
}

