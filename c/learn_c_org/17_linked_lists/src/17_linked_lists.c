#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct node {
    int val; // store value for current element
    struct node* next; // point to next list element
} node_t;

void print_list(node_t* l) {
    // print elements in list
    node_t* current = l;
    while(current != NULL) {
        printf("val: %d\n", current->val);
        current = current->next;
    }
}

void push(node_t *l, int val) {
    // push at the end of the list
    node_t* current = l;
    while(current->next != NULL) {
        current = current->next;
    }

    current->next = (node_t* )malloc(sizeof(node_t)*1);
    assert(current->next != NULL);
    current->next->val = val;
    current->next->next = NULL;
}

void push0(node_t **l, int val) {
    // push as first element in the list
    node_t* head_new = (node_t* )malloc(sizeof(node_t)*1);
    assert(head_new != NULL);
    head_new->val = val;
    head_new->next = *l;
    *l = head_new;
}

int pop(node_t **l) {
    // return first value on list (element is removed)
    int res = -1;
    if (*l != NULL) {
        res = (*l)->val;
        node_t *head_next = (*l)->next;
        node_t *head_current = (*l);
        *l = head_next;
        free(head_current);
    }
    return res;
}

int remove_last(node_t **head) {
    // remove last value on list and return its value
    // return -1 if no node to remove
    // Note: implementation in learn_c.org does not handle case where list has only 1 node
    int res = -1;
    if(*head != NULL) {
        // list not empty
        if((*head)->next == NULL) {
            // only 1 element in list
            res = (*head)->val;
            free(*head);
            *head = NULL;
        } else {
            // regular cases
            node_t* node_prev = *head;
            node_t* node_current = *head;
            while(node_current->next != NULL) {
                node_prev = node_current;
                node_current = node_current->next;
            }
            res = node_current->val;
            node_prev->next = NULL;
            free(node_current);
        }
    }
    return res;
}


int main() {

    // init head of our list
    node_t* head = NULL;
    head = (node_t* )malloc(sizeof(node_t)*1);
    assert(head != NULL);
    head->val = 8;
    head->next = NULL;

    // add an element to our list
    head->next = (node_t* )malloc(sizeof(node_t)*1);
    assert(head->next != NULL);
    head->next->val = 7;
    head->next->next = NULL;

    print_list(head);
    printf("===\n");
    push(head, 6);
    print_list(head);
    printf("===\n");
    push0(&head, 9);
    print_list(head);

    assert(pop(&head) == 9);
    assert(pop(&head) == 8);
    assert(pop(&head) == 7);
    assert(pop(&head) == 6);
    assert(pop(&head) == -1);

    // init head of our list 2
    node_t* head2 = NULL;
    head2 = (node_t* )malloc(sizeof(node_t)*1);
    assert(head2 != NULL);
    head2->val = 5;
    head2->next = NULL;
    push(head2, 10);
    push(head2, 15);

    assert(remove_last(&head2) == 15);
    assert(remove_last(&head2) == 10);
    assert(remove_last(&head2) == 5);
    assert(remove_last(&head2) == -1);
    node_t* head3 = NULL;
    assert(remove_last(&head3) == -1);

    return 0;
}
