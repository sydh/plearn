#include <stdio.h>

int main() {
    int numbers[5];
    numbers[0] = 10;
    numbers[1] = 20;
    numbers[2] = 30;
    numbers[3] = 40;
    numbers[4] = 50;
    printf("numbers @ index 3: %d", numbers[3]);

    return 0;
}