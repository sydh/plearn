cmake_minimum_required(VERSION 3.13)
project(04_multid_arrays)
add_executable(
        04_multid_arrays
        src/04_multid_arrays.c
)
target_compile_options(04_multid_arrays PRIVATE
        $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:
        -Wall -Wextra -pedantic>
        $<$<CXX_COMPILER_ID:MSVC>:
        /W4>)
