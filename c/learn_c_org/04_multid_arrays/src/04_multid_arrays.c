#include <stdio.h>

int main() {
    int foo[1][2][3];
    foo[0][0][2] = 7;

    char vowels[1][5] = {
            {'a', 'e', 'i', 'o', 'u'}
    };

    printf("Vowels: %c - %c - %c - %c - %c\n",
           vowels[0][0],
           vowels[0][1],
           vowels[0][2],
           vowels[0][3],
           vowels[0][4]
    );

    // implicit size for fist dimensions
    char vowels2[][5] = {
            {'A', 'E', 'I', 'O', 'U'},
            {'a', 'e', 'i', 'o', 'u'}
    };

    printf("Vowels (uppercase): %c - %c - %c - %c - %c\n",
           vowels2[0][0],
           vowels2[0][1],
           vowels2[0][2],
           vowels2[0][3],
           vowels2[0][4]
    );

    // init 2 dim array with 1 dim initializer (emit a warning with gcc)
    int a[3][4] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};


    return 0;
}
