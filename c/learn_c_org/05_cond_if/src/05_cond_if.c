#include <stdio.h>

int main() {
    int target = 10;
    // Testing value of target with 'if'
    if (target==10) {
        printf("target value is 10\n");
    }

    int foo = 1;
    int bar = 2;
    // if ... else
    if (foo > bar) {
        printf("foo value is greater than bar value\n");
    } else {
        printf("foo value is smaller than (or equal to) bar value\n");
    }

    // if ... else if ... else
    if (foo > bar) {
        printf("foo value is greater than bar value\n");
    } else if (foo==bar) {
        printf("foo value is the same as bar value\n");
    } else {
        printf("foo value is smaller than bar value\n");
    }

    int foz = 1;
    int baz = 2;
    int moo = 3;

    // AND operator
    if( foz < baz && moo > baz ) {
        printf("foz value is smaller than baz AND moo value is greater than baz value\n");
    }

    // OR operator
    if( foz < baz || moo > baz ) {
        printf("foz value is smaller than baz OR moo value is greater than baz value\n");
    }

    // NOT operator
    if (foz != moo) {
        printf("foz value is not equal to moo value");
    }

    return 0;
}
