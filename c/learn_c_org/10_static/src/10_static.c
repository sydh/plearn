#include <stdio.h>

int runner0() {

    int count = 0;
    count++;
    return count;
}

int runner() {
    static int count = 0;
    count++;
    return count;
}

static void fun(void) {
    // function marked as 'static' make them only avail in the current file
    printf("I'm a static function!\n");
}

int main() {

    // Without static, always return 1
    printf("runner 0: %d\n", runner0());
    printf("runner 1: %d\n", runner0());

    // With static, code\n looks ok
    printf("runner 0: %d\n", runner());
    printf("runner 1: %d\n", runner());

    fun();
}
