#include <stdio.h>

int main() {

    // declare integer variables
    int foo; // compiler will warn about uninit var
    int bar = 1;

    // other types
    short foo0 = 345;
    float foo1 = 12.5f;
    double bar1 = 32.456789;
    long bar2 = 12345689;
    long long bar3 = 1234567890;
    char c0 = 'a';

    printf("foo: %d\n", foo);
    printf("bar: %d\n", bar);

    // exo 01: print the sum of variable a, b, c
    int a = 1;
    int b = 2;
    int c = 3;

    printf("sum of a, b, c: %d", a+b+c);

    return 0;
}
