from django.urls import path

from . import views

# add namespace to prevent collision if multiple apps in the same Django site
# are used
app_name = "polls"

urlpatterns = [
    # arguments
    # route: a string as url pattern
    # view: view to render response
    # name: naming of url
    # kwargs: not used here
    # path("", views.index, name="index"),
    # test with: http://127.0.0.1:8000/1
    # test error with: http://127.0.0.1:8000/AA
    # path("<int:question_id>/", views.detail, name="detail"),
    # path("<int:question_id>/results", views.results, name="results"),
    # test with: http://127.0.0.1:8000/1/vote
    # path("<int:question_id>/vote", views.vote, name="vote"),
    
    # Using generic view
    path("", views.IndexView.as_view(), name="index"),
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("<int:pk>/results/", views.ResultsView.as_view(), name="results"),
    path("<int:question_id>/vote/", views.vote, name="vote"),
]
