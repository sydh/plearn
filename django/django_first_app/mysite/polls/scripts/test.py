from django.test import Client
from django.urls import reverse
from django.test.utils import setup_test_environment

setup_test_environment()

client = Client()
response = client.get("/")
print(response.status_code)

response = client.get(reverse("polls:index"))
print(response.status_code)
print(response.content)
print(response.context["latest_question_list"])

