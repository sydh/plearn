#
# ./manage.py shell scripts
# run with:
# ./manage.py shell < SCRIPT.py

from polls.models import Choice, Question

# Should be empty on 1st run
questions = Question.objects.all()
print(f"Questions: {questions}")

# Create a question
from django.utils import timezone
q = Question(question_text="What's new?", pub_date=timezone.now())

q.save()
print(f"question id: {q.id}")
print(f"question pub date: {q.pub_date}")

q.question_text = "What's up?"
q.save()

questions = Question.objects.all()
print(f"Questions: {questions}")

