#
# ./manage.py shell scripts
# run with:
# ./manage.py shell < SCRIPT.py

import traceback

from polls.models import Question
from django.utils import timezone

questions = Question.objects.all()

# lookup api
q1 = Question.objects.filter(id=1)
print(f"q1: {q1}")

q1_2 = Question.objects.filter(question_text__startswith="What")
print(f"q1_2: {q1_2}")

qy = Question.objects.filter(pub_date__year=timezone.now().year)
print(f"all question published this year: {qy}")

try:
    # Request an ID that does not exist -> exception
    q1_3 = Question.objects.filter(id=999)
except DoesNotExist:
    print(traceback.format_exc())

print("yo")
# pk => Primary Key, identical as id=?
q = Question.objects.get(pk=1)
print("question", q)
print(dir(q))
print("choices", q.choice_set.all())

q.choice_set.create(choice_text="Not much", votes=0)
q.choice_set.create(choice_text="The sky", votes=0)
c = q.choice_set.create(choice_text="Just chilling", votes=0)

print("choice question", c.question)
print("choices", q.choice_set.all())
print("choices count", q.choice_set.count())

c.delete()

print("choices count", q.choice_set.count())

