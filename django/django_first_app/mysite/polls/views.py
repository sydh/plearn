from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse
from django.db.models import F
from django.views import generic
from django.utils import timezone

from .models import Question


class IndexView(generic.ListView):

    template_name = "polls/index.html"
    context_object_name = "latest_question_list"

    def get_queryset(self):

        # Question published before now and ordered by published date
        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by("-pub_date")[:5]


# Note: DetailView expect the primary key from URL to be called pk
class DetailView(generic.DetailView):

    model = Question
    template_name = 'polls/detail.html'

    def __init__(self, *args, **kwargs):

        print("__init__")
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        # Used as a source for get_object
        print("get_query_set")
        return Question.objects.filter(pub_date__lte=timezone.now())

    def get_context_data(self, **kwargs):
        print("get_context_data")
        context = super().get_context_data(**kwargs)
        print("context", context)
        return context

    def get_object(self):
        # Call get_queryset method as a source
        # Return 1 single object
        # See self.kwargs for what is used for filtering

        # https://github.com/django/django/blob/stable/2.2.x/django/views/generic/detail.py

        print("get_object")
        print(self.kwargs)
        obj = super().get_object()
        print("obj", obj)
        return obj

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def index(request):

    # INITIAL VERSION

    # return HttpResponse("Hello world at polls index !")
    
    # FIRST TEMPLATE RENDER

    latest_question_list = Question.objects.order_by("-pub_date")[:5]
    context = {
        "latest_question_list": latest_question_list,
    }
    # template = loader.get_template("polls/index.html")
    # return HttpResponse(template.render(context, request))

    return render(request, "polls/index.html", context)

def detail(request, question_id):

    # return HttpResponse("You're looking at question id %s." % question_id)


    # FIRST TEMPLATE RENDER
    # try:
    #     question = Question.objects.get(pk=question_id)
    # except Question.DoesNotExist:
    #     raise Http404("Question does not exist")

    # context = {"question": question}
    # return render(request, "polls/detail.html", context)

    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/detail.html", {"question": question})

def results(request, question_id):
    # return HttpResponse("You're looking at the results of question id %s." % question_id)

    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/results.html", {"question": question})

def vote(request, question_id):

    # return HttpResponse("You're voting on question id %s." % question_id)

    question = get_object_or_404(Question, pk=question_id)
    try:
        choice_id = request.POST["choice"]
        selected_choice = question.choice_set.get(pk=choice_id)
    except (KeyError, Choice.DoesNotExist):
        # KeyError: for request.POST[...]
        return render(request, "polls/detail.html", {"question": question,
                                                     "error_message": "No selection!"})
    else:
        
        # Naive code -> has race condition if 2 users vote at the same time
        # Solution: https://docs.djangoproject.com/en/2.2/ref/models/expressions/#avoiding-race-conditions-using-f
        # selected_choice.votes += 1

        selected_choice.votes = F("votes")+1
        
        selected_choice.save()
        # Need to return HttpResponseRedirect to prevent data to from being posted twice
        # if user hits the back button
        # reverse: avoid hardcoding an url in the view function
        return HttpResponseRedirect(reverse("polls:results", args=(question_id,)))
