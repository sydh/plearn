import datetime

from django.utils import timezone
from django.test import TestCase
from django.urls import reverse

from .models import Question

# Create your tests here.


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_futur_question(self):

        ftime = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=ftime)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):

        otime = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=otime)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):

        _time = timezone.now() - datetime.timedelta(hours=23,
                                                    minutes=59,
                                                    seconds=59)
        recent_question = Question(pub_date=_time)
        self.assertIs(recent_question.was_published_recently(), True)


def create_question(question_text, days):

    """ Create a question
    """

    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)


class QuestionIndexViewTests(TestCase):

    def test_no_questions(self):

        """ No question, check message
        """

        response = self.client.get(reverse("polls:index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context["latest_question_list"], [])

    def test_past_question(self):

        """ 1 Question in the past
        """

        q = create_question("Past q.", days=-10)
        response = self.client.get(reverse("polls:index"))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context["latest_question_list"],
                                 [q.__repr__()])

    def test_future_question(self):

        """ 1 Question in the future
        """

        q = create_question("Future q.", days=10)
        response = self.client.get(reverse("polls:index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context["latest_question_list"],
                                 [])


class QuestionDetailViewTests(TestCase):

    def test_future_question(self):
        q = create_question("Future q.", days=10)
        url = reverse("polls:detail", args=(q.id,))
        print("url", url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):

        q = create_question("Past q.", days=-2)
        url = reverse("polls:detail", args=(q.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual([response.context["question"]],
                                 [q.__repr__()])

