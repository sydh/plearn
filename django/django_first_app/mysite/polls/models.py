import datetime

from django.db import models
from django.utils import timezone

# Create your models here.
class Question(models.Model):

    # question_text: name of Field + used in db as column name
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date_published")

    # Add this when dealing with interactive prompt or in admin
    # admin is auto gen. by Django
    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class Choice(models.Model):

    # tells Django Choice is related to single Question
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

