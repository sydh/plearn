# Title

> first app: django project

## Install

```
python -m venv django_first_app
pip install --upgrade pip
pip install django==2.1
```

## Usage

```
source django_first_app/bin/activate
python manage.py migrate
python manage.py runserver
```

## Misc

- Store change of our model

```
python manage.py makemigrations polls
```

- View migration as SQL

```
python manage.py sqlmigrate polls 0001
```

- Check migrations (dry run for db)

```
python manage.py check
```

- Run script (eg. Not a very good way)

```
python manage.py shell < polls/scripts/play1.py
```

- Empty db

```
python manage.py flush
```

Note: or use: ```rm db.sqlite3 && python ./manage.py migrate``` 

- Shell

```
python manage.py shell
```

- Create Superuser

```
python manage.py createsuperuser
```

Note: for the test -> login: admin & password: admin 

- Run tests

```
python manage.py test -v 2
```

Note: use --parallel X for fast test

## Contributing

PRs accepted.

## License

Public domain

