use std::fs::File;
use std::io::ErrorKind;
use std::io;
use std::io::Read;
use std::fs;

fn read_username_from_file() -> Result<String, io::Error> {
    let f = File::open("username.txt");
    let mut f = match f {
        Ok(file) => file,
        Err(error) => return Err(error),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

// same as previous but using the ? operator
fn read_username_from_file2() -> Result<String, io::Error> {
  let mut f = File::open("username.txt")?;
  let mut s = String::new();
  f.read_to_string(&mut s)?;
  Ok(s)
}

// same as previous but chained in one line
fn read_username_from_file3() -> Result<String, io::Error> {
  let mut s = String::new();
  File::open("username.txt")?.read_to_string(&mut s);
  Ok(s)
}

// same as previous but with convenient function
fn read_username_from_file4() -> Result<String, io::Error> {
  fs::read_to_string("username.txt")
}

fn main() {
  println!("chap 09 error handling");

  //
  // recoverable error

  let f = File::open("hello.txt");
  // doc
  // struct: https://doc.rust-lang.org/std/fs/struct.File.html
  // open: https://doc.rust-lang.org/std/fs/struct.File.html#method.open
  // return: https://doc.rust-lang.org/std/io/type.Result.html
  // https://doc.rust-lang.org/std/result/enum.Result.html

  /*
  let f = match f {
    // Ok => Result::Ok
    Ok(file) => file,
    // Result::Ok(file) => file,
    Err(error) => panic!("Problem opening the file: {:?}", error),
  };
  */

  let f = match f {
    Ok(file) => file,
    Err(error) => match error.kind() {
      ErrorKind::NotFound => match File::create("hello.txt") {
        Ok(fc) => fc,
        Err(e) => panic!("Problem creating the file: {:?}", e),
      }
      other_error => panic!("Problem opening the file: {:?}", other_error),
    }
  };

  // A better way to write this
  let f2 = File::open("hello.txt").unwrap_or_else(|error| {
    if error.kind() == ErrorKind::NotFound {
      File::create("hello.txt").unwrap_or_else(|error| {
        panic!("Problem creating the file: {:?}", error);
      })
    } else {
      panic!("Problem opening the file: {:?}", error);
    }
  });

  // unwrap & expect

  // unwrap return Ok(...)) if ok else call panic
  // let f2 = File::open("test.txt").unwrap();
  // same here but message for panic is specified
  // let f3 = File::open("test.txt").expect("Failed to open test.txt");

  let user = read_username_from_file().unwrap();
  println!("user: {}", user);

  let user2 = read_username_from_file2().unwrap();
  println!("user2: {}", user2);

  let user3 = read_username_from_file3().unwrap();
  println!("user3: {}", user3);
  
  let user4 = read_username_from_file4().unwrap();
  println!("user4: {}", user4);

  //
  // unrecoverable error
  panic!("crash and burn!!");
}
