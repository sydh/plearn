
// add this so println!(...) can print the struct
#[derive(Debug)]
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user_1(email: String, username: String) -> User
{
    // basic build user function
    User {
      email: email,
      username: username,
      sign_in_count: 1,
      active: true,
    }
}

fn build_user_2(email: String, username: String) -> User
{
    User {
      // Shorthand: no need to specify field: variable
      email,
      username,
      sign_in_count: 1,
      active: true,
    }
}

#[derive(Debug)]
struct Point(i32, i32, i32); 


#[derive(Debug)]
// unit struct (no fields)
struct Unit;


fn main() {
    println!("chap 05: 01_struct");

    //
    // Struct

    let mut u1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someone1"),
        sign_in_count: 1,
        active: true,
    };
    u1.username = String::from("someone_1");

    let u2 = build_user_1(String::from("someone2@example.com"),
        String::from("someone2@example.com"));

    let u3 = build_user_2(String::from("someone3@example.com"),
        String::from("someone3@example.com"));

    let u4 = User {
        active: false,
        ..u3
    };
    
    println!("user u1: {:?}", u1);
    println!("user u2: {:?}", u2);
    
    // Note: do not print u3 because it has been borrowed 
    // when creating u4
    // because String does not implement the Copy trait

    // println!("user u3: {:?}", u3);
    println!("user u4: {:?}", u4);

    let un1 = Unit;
    println!("Unit struct un1: {:?}", un1);

    //
    // Tuple struct
    
    // Note: specify type for fun but this is not mandatory
    let p1: Point = Point(7, 3, 1);
    println!("point p1: {:?}", p1);
    println!("point p1.x: {}", p1.0);


}
