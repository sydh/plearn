
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
  // use &self -> dont want to take ownership
  fn area(&self) -> u32 {
    self.width * self.height
  }

  fn can_hold(&self, other: &Rectangle) -> bool {
    self.width > other.width && self.height > other.height
  }

  fn square(size: u32) -> Rectangle {
    Rectangle {
      width: size, height: size
    }
  }
}

fn main()
{
    println!("chap 05: 02_example");

    // basic area function

    let w1 = 30;
    let h1 = 50;

    println!("the area of the rectangle is {} square pixels.", area(w1, h1));

    // same but this tuples

    let dim = (30, 50);
    println!("the area of the rectangle is {} square pixels.", area_t(dim));
    
    // same with Struct
    let rect = Rectangle {
        width: 30,
        height: 50,
    };
    let rect2 = Rectangle {
        ..rect
    };
    let rect3 = Rectangle {
      width: 10,
      height: 5,
    };

    // Note: would be better to pass a ref of rect as rect is dropped after area_s(...)
    println!("the area of the rectangle 'rect' is {} square pixels.", area_s(rect));
    // Note: rect2 is passed as non mut ref so not dropped after area_s2(...)
    println!("the area of the rectangle 'rect2' is {} square pixels.", area_s2(&rect2));

    // same with Struct + method of Rectangle

    println!("the area of the rectangle 'rect2' is {} square pixels.", rect2.area());
    println!("Can rectangle 'rect2' hold 'rect3': {}", rect2.can_hold(&rect3));

    let rect4 = Rectangle::square(50);
    println!("rect4: {:?}", rect4);

}

fn area(width: u32, height: u32) -> u32
{
    width * height
}

fn area_t(dim: (u32, u32)) -> u32
{
    dim.0 * dim.1
}

fn area_s(rect: Rectangle) -> u32
{
    rect.width * rect.height
}

fn area_s2(rect: &Rectangle) -> u32
{
    rect.width * rect.height
}

