use std::ops::Deref;
use std::ops::DerefMut;

#[derive(Debug)]
struct MyBox<T>(T);

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        // return a reference to the first element in MyBox
        &self.0
    }
}

impl<T> DerefMut for MyBox<T> {
    fn deref_mut(&mut self) -> &mut T {
        // return a mutable ref to the first element in MyBox
        &mut self.0
    }
}

fn hello(name: &str) {
    println!("Hello, {}!", name);
}

fn main() {
    println!("15 smart pointers: 02 Box<T> Trait Deref");
    println!("=========================");

    let x = 5;
    let y = &x;
    let by = Box::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y);
    // Will not compile: cannot compare i32 and reference
    // assert_eq!(5, y);

    // Box<T> implements Trait Deref so can use dereference op: *
    assert_eq!(5, *by);

    let a = 4;
    let ba = MyBox::new(a);
    assert_eq!(4, *ba);
    // Note: *ba => *(ba.deref())
    // Same as previous line
    assert_eq!(4, *(ba.deref()));

    let a2 = 6;
    let mut ba2 = MyBox::new(a2);
    *ba2 += 1;
    // same as:
    *(ba2.deref_mut()) += 1;
    assert_eq!(8, *ba2);
    println!("ba2: {:?}", ba2);

    let m = MyBox::new(String::from("Rust"));
    // Note: Calling hello(&str) with &MyBox
    // Compiler call deref until it gets the required type
    hello(&m);
}
