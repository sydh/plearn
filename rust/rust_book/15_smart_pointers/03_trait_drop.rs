struct MySmartPointer {
    data: String
}

impl Drop for MySmartPointer {
    fn drop(&mut self) {
        println!("Dropping MySmartPointer with data: {}", self.data);
    }
}

fn main() {

    const EXPLICIT_DROP: bool = true;

    println!("15 smart pointers: 03 Trait Drop");
    println!("=========================");

    let sp1 = MySmartPointer { data: String::from("foo") };
    let sp2 = MySmartPointer { data: String::from("bar") };
    
    println!("sp1: {:?}", sp1);
    println!("sp2: {:?}", sp2);
    println!("My smart pointers created...");
    
    // Dropping MySmartPointer early
    match EXPLICIT_DROP {
        true => drop(sp1),
        false => std::mem::drop(sp1), // same as drop(X)
    }

    println!("My smart pointers dropped before the end of main.");
}
