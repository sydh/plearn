use std::rc::Rc;

#[derive(Debug)]
enum List {
    Element(i32, Box<List>),
    Nil,
}

use crate::List::{Element, Nil};

#[derive(Debug)]
enum List2 {
    Element2(i32, Rc<List2>),
    Nil2,
}

use crate::List2::{Element2, Nil2};

fn main() {
    println!("15 smart pointers: 04 Rc<T>");
    println!("=========================");

    // Rc => Reference Counting / Single Thread only

    // Try to share ownership with Box
    // a: List = [5, 10]
    let a = Element(5, 
        Box::new(Element(10,
                Box::new(Nil))));
    // b: List = [3] + a
    let b = Element(3, Box::new(a));
    // Uncomment this to see compile error
    // let c = Element(4, Box::new(a));

    // Now with List2 and Rc pointer
    let a2 = Rc::new(Element2(57, 
        Rc::new(Element2(10,
                Rc::new(Nil2)))));
    println!("count after creating a = {}", Rc::strong_count(&a2));
    println!("a2: {:?}", a2);    
    // Rc::clone => no deep copy, only incr ref counter
    let b2 = Element2(37, Rc::clone(&a2));
    println!("count after creating b = {}", Rc::strong_count(&a2));
    println!("b2: {:?}", b2);    
    {
        let c2 = Element2(47, Rc::clone(&a2));
        println!("count after creating c = {}", Rc::strong_count(&a2));
        println!("c2: {:?}", c2);    
    }

    println!("count after c goes out of scope = {}", Rc::strong_count(&a2));
    
    // println!("b2: {:?}", b2);    
    // println!("c2: {:?}", c2);    
}
