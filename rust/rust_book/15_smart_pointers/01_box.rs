// First attempt to define List
// Does not compile: List0 is recursive and size is not known @ compile time
/*
enum List0 {
    Cons0(i32, List0),
    Nil,
}

use crate::List0::{Cons0, Nil};
*/

// Note: Cons is a variant of the Enum
// Could be renamed (eg. Pair(i32, Box<List>))

#[derive(Debug)]
#[derive(PartialEq)]
enum List {
    Cons(i32, Box<List>),
    Nil,
}

use crate::List::{Cons, Nil};

fn main() {
    println!("15 smart pointers: Box<T>");
    println!("=========================");

    // Declare a Box<i32>
    // Store i32 value on the heap, Box (pointer) is on the stack
    let b0 = Box::new(3);
    let b1: Box<i32> = Box::new(5);
    println!("b0 = {}", b0);
    println!("b1 = {}", b1);
    println!("b1 = {:?}", b1);

    // Use of Box
    // 1. Enabling recursive types
    // recursive type => One type whose size can’t be known at compile time

    // 1. Example: Cons list    

    // first attempt: see List0 comments
    // let list0 = Cons0(1, Cons0(2, Cons0(3, Nil))); 
    
    let list_null = Nil;
    let list1 = Cons(7, 
        Box::new(Cons(2, 
                Box::new(Cons(3, 
                        Box::new(Nil)))))); 

    println!("list null: {:?}", list_null);
    println!("list1: {:?}", list1);
    
    if list_null == Nil {
        println!("list list_null is empty");
    }
    // Note: same test but using List::[...]
    if list1 == List::Nil {
        println!("list1 is empty");
    }
    else {
        println!("list1 is not empty");
    }

    // First value in list1
    let mut fv: i32 = -1;
    // let mut fv2: i32 = -1;
    // match
    match list1 {
        Cons(_fv, _) => fv = _fv,
        _ => (),
    }
    println!(" [match] list1 first value: {}", fv);
    // if let
    if let List::Cons(value,_) = list1 {
        fv = value;
    }
    println!("[if let] list1 first value: {}", fv);
    // if let v2: Not ok -> Nil case not covered -> compile error
    // let List::Cons(fv2, _) = list1;
    // println!("[if let v2] list1 first value: {}", fv2);

}
