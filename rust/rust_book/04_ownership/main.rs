fn main() {
  println!("Hello, world!");

  // stack versus heap
  // stack: LIFO - fixed size data
  // heap: unkown size @ compile time - allocate memory
  // -- access --
  // stack: faster than heap
  // heap: slow (allocation)

  // ownership rules
  // each value in Rust has a var called its owner
  // Ther can only be one owner at a time
  // When owner out of scope, value is dropped

  // Use String as it is alloc on the heap
  let mut s = String::from("hello");
  s.push_str(" world !!");
  println!("s: {}", s);

  // When s goes out of scope, the function drop is called

  // Move

  let x = 5;
  let y = x;
  println!("x: {}, y: {}", x, y);

  let s1 = String::from("hello");
  let s2 = s1; 
  // Note: will not work, s2 take ownership over s1 and s1 has been dropped
  /* println!("s1: {}", s1); */
  // This line is ok
  println!("s2: {}", s2);

  // Clone

  let s1c = String::from("hello");
  // s1c is cloned so owner is kept
  let s2c = s1c.clone(); 

  println!("s1c: {}, s2c: {}", s1c, s2c);

  // Ownership and functions
  let s1f = String::from("foobar");
  // func will take ownership of s1f
  take_ownership(s1f);
  // Will not work as s1f has been dropped in take_ownership
  // println!("s1f: {}", s1f);

  let mut s2f = give_ownership();
  s2f = take_and_give_back(s2f);
  println!("s2f: {}", s2f);

  // so what if we want to use var but not take ownership?
  // -> References

  println!("lenght of s2f: {}", calc_len(&s2f));  

  // Mutable references
  let mut sr0 = String::from("Good");
  let _sr0a = &sr0;
  // Ok as multiple non mutable ref are ok
  let _sr0b = &sr0;
  let sr1 = &mut sr0;
  // Will not work:, only 1 mutable ref allowed
  // let sr2 = &mut sr1; 

  change_str(sr1);
  println!("sr0: {}", sr0);

  // Slice type

  // initial version of first_word func -> return integer
  println!("First word of sr0: {}", first_word_1(&sr0, b' '));

  let slice1 = &sr0[0..2];
  let slice2 = &sr0[2..];
  let slice3 = &sr0[..];
  println!("slice1: {}", slice1);
  println!("slice2: {:?}", slice2);
  println!("slice3: {:?}", slice3);

  // better -> return slice
  println!("First word of sr0: {}", first_word_2(&sr0, b' '));
  
  let sl1 = "Foo bar";
  let so1 = "Foo bar";
  let so1sl1 = &so1[..5];
  // better -> take slice as argument and return slice
  println!("First word of string litteral of {}: {}", sl1, first_word(sl1, b' '));
  println!("First word of slice of {}: {}", so1sl1, first_word(so1sl1, b' '));

  // other slices
  let a = [1, 2, 3, 4];
  let asl1 = &a[1..];
  println!("slice of array a {:?}, {:?}", a, asl1);
}

fn take_ownership(_s: String)
{
  println!("take ownership...");
}

fn give_ownership() -> String
{
  let s = String::from("foobar2");
  s
}

fn take_and_give_back(s: String) -> String
{
  println!("take ownership and give back...");
  s
}

fn calc_len(s: &String) -> usize
{
  s.len()
}

fn change_str(s: &mut String)
{
  s.push_str(" day !");
}

fn first_word_1(s: &String, c: u8) -> usize
{
  // return index at the end of first word
  // delimited by a space character
  // initial naive version of first word function

  let bytes = s.as_bytes();
  for(i, &item) in bytes.iter().enumerate()
  {
    // if item == b' '
    if item == c
    {
      return i;
    }
  }

  return s.len();
}

fn first_word_2(s: &String, c: u8) -> &str
{
  // same as first_word_1 but return a slice
    
  let bytes = s.as_bytes();
  for(i, &item) in bytes.iter().enumerate()
  {
    // if item == b' '
    if item == c
    {
      return &s[..i];
    }
  }

  return &s[..];
}

fn first_word(s: &str, c: u8) -> &str
{
  // string literals are slice
  // let s = "Hello wolrd"; -> type of s is &str
  
  let bytes = s.as_bytes();
  for(i, &item) in bytes.iter().enumerate()
  {
    // if item == b' '
    if item == c
    {
      return &s[..i];
    }
  }

  return &s[..];
}
