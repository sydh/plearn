fn largest(l: &[i32]) -> i32 {
    // Cannot pass type like: [i32]
    // See: rustc --explain E0277
    // Passing ref fix the issue
    let mut largest: i32 = l[0];

    // https://doc.rust-lang.org/std/vec/struct.Vec.html#method.iter
    //
    for &number in l.iter() {
        if number > largest {
            largest = number;
        }
    }
    largest
}

// Note: compilation error
// require PartialOrd trait for T
/*
fn glargest<T>(list: &[T]) -> T {
    let mut largest = list[0];
    for &item in list.iter() {
      if item > largest {
        largest = item;
      }
    }
}
*/

#[derive(Debug)]
struct Point<T> {
    x: T,
    y: T,
}

// Note:
// impl<T> so rust can identify T as generic
impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

// Note:
// here no need to specify T as fn will only
// be available for Point<f32>
impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}


#[derive(Debug)]
struct Point2<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point2<T, U> {
    // declare new types V & W for mixup
    fn mixup<V, W>(self, other: Point2<V, W>) -> Point2<T, W> {
        Point2 {
            x: self.x,
            y: other.y,
        }
    }
}

enum MyEnum<T> {
    Foo(T),
    Bar(T),
}

fn main() {
    println!("chap 10: 01_generic");

    let number_list = vec![34, 50, 25, 100, 65];
    let mut largest0 = number_list[0];

    for number in number_list {
        if number > largest0 {
            largest0 = number;
        }
    }
    println!("largest: {}", largest0);

    let number_list = vec![102, 34, 6000, 89, 54, 2, 43, 8];

    println!("largest: {}", largest(&number_list));
    println!("number_list: {:?}", number_list);

    let number_list = vec![102, 34, 60, 89, 54, 2, 43, 866];

    // Note: will not compile
    // T type need trait std::cmp::PartialOrd
    // println!("largest: {}", glargest(&number_list));

    let p1 = Point { x: 3, y: 15 };
    println!("Point p1: {:?}", p1);
    let p2 = Point {
        x: 3.1245,
        y: 15.67,
    };
    println!("Point p2: {:?}", p2);

    let p3 = Point2 { x: 2.3, y: 89 };
    println!("Point p3: {:?}", p3);

    println!("Point p1.x: {}", p1.x());
    // Uncomment to see compile error: method `...` not found for Point<T> 
    // println!("Point p1 distance from origin: {}", p1.distance_from_origin());
    println!("Point p2 distance from origin: {}", p2.distance_from_origin());

    let p4_1 = Point2 {x: 5, y: 10.4};
    let p4_2 = Point2 {x: "Hello", y: 'c'};
    // Note: type is not mandatory but here for readability 
    let p4_3: Point2<i32, char> = p4_1.mixup(p4_2);
    println!("p3.x: {}, p3.y: {}", p4_3.x, p4_3.y); 
}
