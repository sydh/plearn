// default Trait definition
/*
pub trait Summary {
    fn summarize(&self) -> String;
}
*/

// Trait with default impl
pub trait Summary {
    fn summarize(&self) -> String {
        String::from("(Read more...)")
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{} by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

#[derive(Debug)]
pub struct Toots {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

// use default implementation of summarize for Toots
// if no default, compile error
impl Summary for Toots {}

pub fn notify(item: impl Summary) {
    println!("[NOTIFY] Breaking news! {}", item.summarize());
}

// Note: require a type with 2 Traits: Summary && Debug
pub fn notify2(item: impl Summary + std::fmt::Debug) {
    println!("[NOTIFY2] Breaking news! {} - {:?}", item.summarize(), item);
}

// Define types T and U, some_function have arguments t & u
// Note: line is quite long!
fn some_function<T: std::fmt::Display + Clone, U: Clone + std::fmt::Debug>(_t: T, _u: U) -> i32 {
    8
}

// Use where clause after function to improve readability
fn some_function2<T, U>(_t: T, _u: U) -> i32
where
    T: std::fmt::Display + Clone,
    U: Clone + std::fmt::Debug,
{
    7
}

// Same as notify2 but using where clause
pub fn notify3<T>(item: T)
    where T: Summary + std::fmt::Debug {
    
    println!("[NOTIFY3] Breaking news! {} - {:?}", item.summarize(), item);
}

// Function return a value with type that implement trait Summary
fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("[RET TRAIT] horse_ebooks"),
        content: String::from("of course, as you probably already know, people"),
        reply: false,
        retweet: false,
    }
}

// implement working version of largest function
// see 01_generic.rs "glargest" function
// require T to have trait PartialOrd (for comparison)
// + trait Copy (first line is a copy)
fn largest<T>(list: &[T]) -> T 
where T: std::cmp::PartialOrd + Copy {
    let mut largest = list[0]; // copy
    for &item in list.iter() {
      if item > largest { // cmp
        largest = item;
      }
    }
    largest
}

// another implementation with trait Clone
// thus this can work with Vec<String>
// Not optimal: clone -> potential heap alloc
fn largest_clone<T>(list: &[T]) -> T 
where T: std::cmp::PartialOrd + Clone {
    let mut largest = list[0].clone(); // clone
    for item in list.iter() {
      if item > &largest { // cmp
        largest = item.clone();
      }
    }
    largest
}

// another implementation
// No need for Copy || Clone + no heap alloc
fn largest_ref<T>(list: &[T]) -> &T
where T: std::cmp::PartialOrd {
    let mut largest = &(list[0]);
    for item in list.iter() {
        // cmp with ref are ok
        if item > largest {
            largest = item;
        }
    }
    largest
}

// Conditionall method impl

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    // implement new for all types
    fn new(x: T, y: T) -> Self {
        Self {
            x: x,
            y: y,
        }
    }
}

impl<T: std::fmt::Display + std::cmp::PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x > self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}

// same as previous but using where
impl<T> Pair<T> 
  where T: std::fmt::Display + std::cmp::PartialOrd {
    fn cmp_display2(&self) {
        if self.x < self.y {
            println!("The lowest member is x = {}", self.x);
        } else {
            println!("The lowest member is y = {}", self.y);
        }
    }
}

fn main() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from("of course, as you probably already know, people"),
        reply: false,
        retweet: false,
    };

    let _tweet1 = Tweet {
        username: String::from("USERNAME"),
        content: String::from("CONTENT"),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());

    let toots = Toots {
        username: String::from("foo"),
        content: String::from("bar"),
        reply: false,
        retweet: false,
    };

    let toots1 = Toots {
        username: String::from("foo1"),
        content: String::from("bar1"),
        reply: false,
        retweet: false,
    };

    println!("1 new toots: {}", toots.summarize());

    let article = NewsArticle {
        headline: String::from("Penguins win the Stanley Cup Championship!"),
        location: String::from("Pittsburgh, PA, USA"),
        author: String::from("Iceburgh"),
        content: String::from(
            "The Pittsburgh Penguins once again are the best
        hockey team in the NHL.",
        ),
    };

    println!("New article available! {}", article.summarize());

    notify(tweet);
    notify(article);
    let _i: u32 = 456;
    // uncomment to see compile error as
    // trait Summary is not implemented for type u32
    // notify(_i);

    notify2(toots);
    // uncomment to see compile error as
    // Debug trait is not implemented for Tweet
    // notify2(_tweet1);

    notify3(toots1);

    notify(returns_summarizable());

    let v = vec![17, 42, 9, 6, 99];
    println!("largest of v: {}", largest(&v));
    let vc = vec!['y', 'a', 'g', 'z'];
    println!("largest of vc: {}", largest(&vc));

    let vs = vec![String::from("aaa"), String::from("aaz"), String::from("aay")];
    println!("largest of vs: {}", largest_clone(&vs));

    // some test with String ref
    let s1 = String::from("foo");
    let s2 = String::from("foo");
    let s3 = String::from("bar");
    // plain comparison
    println!("s1 ({}) == s2 ({}) ? {}", s1, s2, s1==s2); 
    println!("s1 ({}) == s3 ({}) ? {}", s1, s3, s1==s3);
    // comparison using ref
    println!("&s1 ({}) == &s2 ({}) ? {}", &s1, &s2, &s1==&s2); 
    println!("&s1 ({}) == &s3 ({}) ? {}", &s1, &s3, &s1==&s3); 

    println!("largest of vs: {}", largest_ref(&vs));

    // Pair<T>
    let p1 = Pair { x: 3.45f32, y: 6.57f32 };
    p1.cmp_display();
    p1.cmp_display2();
    let tw1 = Tweet { username: String::from("tw1 u"), 
        content: String::from("tw1 c"), 
        reply: false, retweet: false 
    };
    let tw2 = Tweet { username: String::from("tw2 u"), 
        content: String::from("tw2 c"), 
        reply: false, retweet: false 
    };
    let p2 = Pair { x: tw1, y: tw2 };
    // uncomment to see compile error
    // Tweet does not impl traits: Display + PartialOrd
    // p2.cmp_display();

}
