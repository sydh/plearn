
// Note: does not compile
// expected lifetime parameter for return type
/*
fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
*/

// Code compile with lifetime annotations
// Rust compiler can now check ref lifetimes
fn longest<'b>(x: &'b str, y: &'b str) -> &'b str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

// Note: does not compile
// Lifetime of function argument need to 
// match lifetime of return (if return is a ref)
/*
fn longest2<'b>(x: &'b str, y: &str) -> &'b str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
*/

fn foo<'a>() {

    // Playing with lifetime
    let i_a1 = 3;
    let _i0: &i32 = &i_a1; // reference
    let _i1: &'a i32; // a reference with explicit lifetime

    let _i2: &'a mut i32; // a mutable reference with explicit lifetime
}

// Struct with ref
// need lifetime annotation

#[derive(Debug)]
struct ImportantStuff<'a> {
    part: &'a str,
}

impl<'a> ImportantStuff<'a> {
    fn level(&self) -> i32 {
        3
    }

    // lifetime elision
    fn announce(&self, announcement: &str) -> &str {
        println!("Attention please! -> {}", announcement);
        self.part
    }
}

// Lifetime Elision
// Lifetime are not always explicitely required

fn first_word(s: &str) -> &str {
    // Function signature with lifetime
    // fn first_word<'a>(s: &'a str) -> &'a str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

// Generics + Lifetimes

use std::fmt::Display;

fn longest_with_announce<'a, T>(x: &'a str, y: &'a str, ann: T) -> &'a str
    where T: Display {
        println!("Attention please! -> {}", ann);
        if x.len() > y.len() {
            x
        } else {
            y
        }
}


fn main() {

    // Lifetime goal -> prevent dangling reference

    foo();

    let _string1 = String::from("abcd");
    let _string2 = "xyz";

    // longest: test 1 (same lifetime)
    let result = longest(_string1.as_str(), _string2);
    println!("The longest string is {}", result);

    // longest: test 2 (different lifetime but lifetime of
    // string1 > lifetime of string2/result
    let string1 = String::from("long string is long");

    {
        let string2 = String::from("xyz");
        let result = longest(string1.as_str(), string2.as_str());
        println!("The longest string is {}", result);
    }
    
    // longest: test 3 (Does not compile
    // string1 & result have the same lifetime
    // after longest, result of lifetime is !=
    /*
    let string1 = String::from("long string is long");
    let result;
    {
        let string2 = String::from("xyz");
        result = longest(string1.as_str(), string2.as_str());
    } // Note: string2 is dropped here, so result is not valid anymore
    println!("The longest string is {}", result);
    */

    let novel = String::from("Once upon a time in nowhere. Foo bar");
    // Note: type specified for clarity (not mandatory)
    let first_sentence: &str = novel.split('.')
        .next()
        .expect("Could not find a '.'");
    let is = ImportantStuff{ part: first_sentence };
    println!("Struct is: {:?}", is);
    println!("is level: {}", is.level());
    println!("is ann: {}", is.announce("Hello there!"));

    println!("first word of novel: `{}` is `{}`", novel, first_word(&novel));

    // Static lifetime
    // Ref can live for the entire duration of the program
    let s: &'static str = "I have a static lifetime.";

    println!("static s: {}", s);

    println!("{}", longest_with_announce("foo", "bar_bar", "Wololo!"));
}

