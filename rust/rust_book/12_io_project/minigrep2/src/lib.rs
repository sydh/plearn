use std::env;
use std::error::Error;
use std::fs;

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    // More idiomatic rust code (than parse_config fn)

    pub fn new(args: &[String]) -> Result<Self, &str> {
        if args.len() < 3 {
            return Err("Not enough arguments: CMD 'query' 'filepath'");
        }
        let query = args[1].clone();
        let filename = args[2].clone();

        // let mut case_sensitive = env::var("CASE_INSENSITIVE").is_err();
        let key = "CASE_INSENSITIVE";
        /*
        match env::var(key) {
            Ok(v) => println!("{}: {:?}", key, v),
            Err(e) => println!("couldn't interpret {}: {}", key, e),
        }
        */

        let case_sensitive;
        match env::var(key) {
            Ok(v) => case_sensitive = !(v != "0"),
            Err(_) => case_sensitive = false,
        }

        // println!("case_sensitive: {}", case_sensitive);

        // Note: init Config is ok if variable have the same
        // name as the struct fields
        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }

    pub fn new_old(args: &[String]) -> Self {
        if args.len() < 3 {
            panic!("Not enough arguments!");
        }
        let query = args[1].clone();
        let filename = args[2].clone();

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Config {
            query,
            filename,
            case_sensitive,
        }
    }
}

pub fn parse_config(args: &[String]) -> Config {
    // Clone value for simplicity
    let query = args[1].clone();
    let filename = args[2].clone();
    let case_sensitive = true;

    Config {
        query,
        filename,
        case_sensitive,
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    // println!("Content:\n{}\n#####", contents);

    let results = if config.case_sensitive {
        search(&config.query, &contents)
    } else {
        search_case_insensitive(&config.query, &contents)
    };

    for line in results {
        println!("{}", line);
    }
    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();
    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }
    results
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut results = Vec::new();
    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }
    results
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn one_result_insensitive() {
        let query = "rUSt";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
