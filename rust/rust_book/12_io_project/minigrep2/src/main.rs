use std::env;
use std::process;

use minigrep2::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    // let _config0 = minigrep2::parse_config(&args);
    // let _config1 = Config::new_old(&args);
    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    println!("[Config] query: {}", config.query);
    println!("[Config] filename: {}", config.filename);

    /*
    let contents = fs::read_to_string(config.filename)
        .expect("Something went wrong reading the file");

    println!("[File] file contents:\n{}\n#####\n", contents);
    */

    if let Err(e) = minigrep2::run(config) {
        eprintln!("Reading file error: {}", e);
        process::exit(2);
    }
}
