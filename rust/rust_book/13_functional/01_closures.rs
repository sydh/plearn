use std::thread;
use std::time::Duration;
use std::collections::HashMap;
use std::collections::hash_map::Entry;


fn simulated_expensive_workout(intensity: u32) -> u32 {
    println!("Calculating slowly...");
    thread::sleep(Duration::from_secs(1));
    intensity
}

// Version 1: naive code
fn generate_workout(intensity: u32, random_number: u32) {
    if intensity < 25 {
        println!("Today, do {} pushups!", simulated_expensive_workout(intensity));
        println!("Next, do {} situps!", simulated_expensive_workout(intensity));

    } else {
        if random_number == 3 {
            println!("Please do a break today!");
        } else {
            println!("Today, run for {} minutes", simulated_expensive_workout(intensity));
        }
    }
}

// Version 2: refactor using function (remove same function called multiple time)
fn generate_workout2(intensity: u32, random_number: u32) {
    let expensive_result = simulated_expensive_workout(intensity);
    if intensity < 25 {
        println!("Today, do {} pushups!", expensive_result);
        println!("Next, do {} situps!", expensive_result);

    } else {
        if random_number == 3 {
            println!("Please do a break today!");
        } else {
            println!("Today, run for {} minutes", expensive_result);
        }
    }
}

// Version 3: closure
fn generate_workout3(intensity: u32, random_number: u32) {
    // Closure that takes 1 arg (ie. num) 
    let expensive_closure = |num| {
        println!("[Closure] Calculating slowly...");
        thread::sleep(Duration::from_secs(1));
        num
    };

    // Closure with type annotation
    // Most of time, types are inferred by compiler
    let expensive_closure_t = |num: u32| -> u32 {
        println!("[Closure] Calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    };

    if intensity < 25 {
        println!("Today, do {} pushups!", expensive_closure(intensity));
        println!("Next, do {} situps!", expensive_closure(intensity));

    } else {
        if random_number == 3 {
            println!("Please do a break today!");
        } else {
            println!("Today, run for {} minutes", expensive_closure_t(intensity));
        }
    }
}

// Struct with closure as for 1 field
struct Cacher<T>
    where T: Fn(u32) -> u32
{
    calculation: T,
    value: Option<u32>,
}

impl<T> Cacher<T>
    where T: Fn(u32) -> u32 {

    pub fn new(calc: T) -> Self {
        Cacher {
            calculation: calc,
            value: None,
        }
    }

    pub fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            Some(v) => v,
            None => {
                let v = (self.calculation)(arg);
                self.value = Some(v);
                v
            },
        }
    }
}

// Version 4: closure + cache
fn generate_workout4(intensity: u32, random_number: u32) {
    // Closure that takes 1 arg (ie. num) 
    let expensive_closure = |num| {
        println!("[Closure] Calculating slowly...");
        thread::sleep(Duration::from_secs(1));
        num
    };

    // No type annotation for cc
    let mut cc = Cacher::new(expensive_closure);
    // With partial type annotation
    let cc1: Cacher<_> = Cacher::new(expensive_closure);
    println!("cc1.value: {:?}", cc1.value);

    // Closure with type annotation
    // Most of time, types are inferred by compiler
    let _expensive_closure_t = |num: u32| -> u32 {
        println!("[Closure] Calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    };

    if intensity < 25 {
        println!("Today, do {} pushups!", cc.value(intensity));
        println!("Next, do {} situps!", cc.value(intensity));

    } else {
        if random_number == 3 {
            println!("Please do a break today!");
        } else {
            println!("Today, run for {} minutes", cc.value(intensity));
        }
    }
}

// Exo 01: Better Cacher (with hashmap)
struct Cacher2<T>
    where T: Fn(u32) -> u32
{
    calculation: T,
    // value: Option<u32>,
    map: HashMap<u32, u32> 
}

impl<T> Cacher2<T>
    where T: Fn(u32) -> u32,
{

    pub fn new(calc: T) -> Self {
        Cacher2 {
            calculation: calc,
            // value: None,
            map: HashMap::new(),
        }
    }

    pub fn value(&mut self, arg: u32) -> u32 {
        
        // get hashmap entry for argument
        let e = self.map.entry(arg);
        match e {
            // Already in HashMap -> return value
            Entry::Occupied(entry) => {
                println!("In HashMap");
                *entry.get()
            }, 
            // Not in HashMap -> store it and return computed value
            Entry::Vacant(entry) => { 
                println!("Not in HashMap");
                let v2 = (self.calculation)(arg);
                // Store in HashMap
                entry.insert(v2);
                v2
            },
        }
    }
}

// Version 5: closure + cache v2
fn generate_workout5(intensity: u32, random_number: u32) {
    // Closure that takes 1 arg (ie. num) 
    let expensive_closure = |num| {
        println!("[Closure] Calculating slowly...");
        thread::sleep(Duration::from_secs(1));
        num
    };

    // No type annotation for cc
    let mut cc = Cacher2::new(expensive_closure);
    // With partial type annotation
    let cc1: Cacher2<_> = Cacher2::new(expensive_closure);
    println!("cc1.map: {:?}", cc1.map);

    // Closure with type annotation
    // Most of time, types are inferred by compiler
    // let expensive_closure_t = |num: u32| -> u32 {
    //     println!("[Closure] Calculating slowly...");
    //     thread::sleep(Duration::from_secs(2));
    //     num
    // };

    println!("== generate_workout5 ==");

    if intensity < 25 {
        println!("Today, do {} pushups!", cc.value(intensity));
        println!("Next 1, do {} pushups!", cc.value(intensity));
        println!("Next 2, do {} situps!", cc.value(intensity+1));

    } else {
        if random_number == 3 {
            println!("Please do a break today!");
        } else {
            println!("Today, run for {} minutes", cc.value(intensity+2));
        }
    }
}

// Exo 01: Better Cacher (with hashmap)
struct Cacher3<K, V, F> where 
    K: Eq + std::hash::Hash, 
    F: Fn(K) -> V
{
    calculation: F,
    map: HashMap<K, V> 
}

impl<K, V, F> Cacher3<K, V, F> where 
    K: Eq + std::hash::Hash + Copy,
    V: Copy,
    F: Fn(K) -> V
{
    
    pub fn new(calc: F) -> Self {
        Cacher3 {
            calculation: calc,
            // value: None,
            map: HashMap::<K, V>::new(),
        }
    }

    pub fn value(&mut self, arg: K) -> V {
        
        // get hashmap entry for argument
        let e = self.map.entry(arg);
        match e {
            // Already in HashMap -> return value
            Entry::Occupied(entry) => {
                println!("In HashMap");
                *entry.get()
            }, 
            // Not in HashMap -> store it and return computed value
            Entry::Vacant(entry) => { 
                println!("Not in HashMap");
                let v2 = (self.calculation)(arg);
                // Store in HashMap
                entry.insert(v2);
                v2
            },
        }
    }
}

// Version 6: closure + cache v3
fn generate_workout6(intensity: u32, random_number: u32) {
    // Closure that takes 1 arg (ie. num) 
    let expensive_closure = |num| {
        println!("[Closure] Calculating slowly...");
        thread::sleep(Duration::from_secs(1));
        num
    };

    let expensive_closure2 = |s: &str| {
        println!("[Closure][str] Calculating slowly...");
        thread::sleep(Duration::from_secs(1));
        s.len()
    };

    // No type annotation for cc
    let mut cc = Cacher3::new(expensive_closure);
    // With partial type annotation
    let cc1: Cacher3<_, _, _> = Cacher3::new(expensive_closure);
    println!("cc1.map: {:?}", cc1.map);

    let mut cc2 = Cacher3::new(expensive_closure2);

    println!("== generate_workout6 ==");

    if intensity < 25 {
        println!("Today, do {} pushups!", cc.value(intensity));
        println!("Next 1, do {} pushups!", cc.value(intensity));
        println!("===");
        println!("Next 2, do {} situps!", cc2.value("situps!"));
        println!("Next 3, do {} situps!", cc2.value("situps!"));
        println!("Next 4, do {} situps!", cc2.value("More situps!"));

    } else {
        if random_number == 3 {
            println!("Please do a break today!");
        } else {
            println!("Today, run for {} minutes", cc.value(intensity+2));
        }
    }
}



fn main() {
    println!("13 - closures");
    
    let simulated_user_specified_value = 10;
    let simulated_random_number = 7;

    generate_workout(
        simulated_user_specified_value,
        simulated_random_number
    );

    generate_workout2(
        simulated_user_specified_value,
        simulated_random_number
    );

    generate_workout3(
        simulated_user_specified_value,
        simulated_random_number
    );

    generate_workout4(
        simulated_user_specified_value,
        simulated_random_number
    );

    generate_workout5(
        simulated_user_specified_value,
        simulated_random_number
    );

    generate_workout6(
        simulated_user_specified_value,
        simulated_random_number
    );

    // move keyword
    // force closure to take ownership of values it uses in the env
    
    let v1 = vec![1, 2, 3, 4];
    // let equal_to_x1 = |z: Vec<i32>| z.len();
    let equal_to_x2 = move |z| z == v1;
    // uncomment this to see compile error
    // println!("v1: {:?}", v1);
    let v2 = vec![1, 2, 3, 4];
    assert!(equal_to_x2(v2));


}
