use std::env;
use std::process;

use minigrep3::Config;

fn main() {
    // let args: Vec<String> = env::args().collect();

    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    println!("[Config] query: {}", config.query);
    println!("[Config] filename: {}", config.filename);

    if let Err(e) = minigrep3::run(config) {
        eprintln!("Reading file error: {}", e);
        process::exit(2);
    }
}
