#[derive(PartialEq, Debug)]
struct Shoe {
    size: u32,
    style: String,
}

fn shoes_in_my_size(shoes: Vec<Shoe>, shoe_size: u32) -> Vec<Shoe> {
    shoes
        .into_iter()
        .filter(|s| s.size == shoe_size)
        .collect()
}

struct Counter {
    count: u32
}

impl Counter {
    fn new() -> Self {
        Counter { count: 0 }
    }
}

// implement next() for Counter
impl Iterator for Counter {
    type Item = u32;

    // fn next(&mut self) -> Option<u32> {
    fn next(&mut self) -> Option<Self::Item> {

        self.count += 1;
        if self.count < 6 {
            Some(self.count)
        }
        else {
            None
        }

    }
}

fn main() {
    
    let v1 = vec!(1, 2, 3);
    println!("v1: {:?}", v1);

    // iterator over v1
    let v1_iter = v1.iter();
    println!("v1 iter: {:?}", v1_iter);

    for val in v1_iter {
        println!("val: {}", val);
    }

    // calling next() on iterator
    // next() modify iterator so require a mutable iterator
    let mut v1_iter2 = v1.iter(); // iterator over mutable reference
    assert_eq!(v1_iter2.next(), Some(&1));
    assert_eq!(v1_iter2.next(), Some(&2));
    assert_eq!(v1_iter2.next(), Some(&3));
    assert_eq!(v1_iter2.next(), None);

    let mut v1m = vec!(1, 2, 3);
    println!("v1m: {:?}", v1m);
    // iter over mutable ref
    let mut v1m_iter3 = v1m.iter_mut();
    // next() -> Option<&mut {integer}>
    let o1 = v1m_iter3.next();
    // unwrap && increment
    let i1_mut_ref = o1.unwrap();
    *i1_mut_ref += 1;
    println!("v1m[0]: {}", v1m[0]);
    assert_eq!(v1m[0], 2);

    // method that consume iterator
    // note: sum takes ownership of iterator
    let total_v1m: i32 = v1m.iter().sum();
    assert_eq!(total_v1m, 7);

    // methods that produce other iterator
    // note: generate a warning because map is lazy 
    v1m.iter().map(|x| x+1);
    let v2: Vec<_> = v1m.iter().map(|x| x+1).collect();
    println!("v2: {:?}", v2); 

    let shoes = vec![
        Shoe { size: 10, style: String::from("sneaker") },
        Shoe { size: 13, style: String::from("sandal") },
        Shoe { size: 10, style: String::from("boot") },
    ];

    let shoe_my_size = shoes_in_my_size(shoes, 10);

    println!("shoe_my_size: {:?}", shoe_my_size);

    let counter1 = Counter::new();
    for i in counter1 {
        println!("counter: {}", i);
    }

    // mixing iterators
    let counter2 = Counter::new();
    let counter3 = Counter::new();
    // Note: zip returns None when either of its input iterators return None.
    for i in counter2.zip(counter3.skip(1)).enumerate() {
        println!("tpl: {:?}", i);
    }
}
