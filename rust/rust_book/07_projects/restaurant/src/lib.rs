// define a module
mod front_of_house {

  // module can contain: struct, enums, constants, traits or another module
  
  // another module inside the module front_of_house
  // note: module hosting is public and can be used from outside
  pub mod hosting {
    pub fn add_to_waitlist() {}
    fn seat_at_table() {}

    #[derive(Debug)]
    pub enum Vegetable {
      Tomato,
      Carrot,
      Salad,
    }
  }

  // struct Stock is private
  struct Stock {
    vegetable_count: u32,
    fruit_count: u32,
  }


  // another module
  pub mod serving {
    fn take_order() {}
    fn serve_order() {}
    fn take_payment() {}
  }
}

fn serve_order() {}

mod back_of_house {

  // define struct as public
  pub struct Breakfast {
    // 2 fields: 1 public and 1 private
    pub toast: String,
    seasonal_fruit: String,
  }

  impl Breakfast {
    pub fn summer(toast: &str) -> Breakfast {
      Breakfast {
        toast: String::from(toast),
        seasonal_fruit: String::from("peaches"),
      }
    }
  }

  // enum is public as well
  // all of its variant are then public
  #[derive(Debug)]
  pub enum Appetizer {
    Soup,
    Salad,
  }

  fn fix_incorrect_order() {
    cook_order();
    // super for relative path with ..
    super::serve_order();
  }

  fn cook_order() {}
}

// shortcut -> can directly use hosting::[...]
use front_of_house::hosting;

pub fn eat_at_restaurant() {
  // calling add_to_waitlist with absolute path
  crate::front_of_house::hosting::add_to_waitlist();
  // calling add_to_waitlist with relative path
  front_of_house::hosting::add_to_waitlist();

  // Rye => "Seigle"
  let mut meal = back_of_house::Breakfast::summer("Rye");
  meal.toast = String::from("Wheat");
  println!("I'd like {} toast please!", meal.toast);
  // Warning: will not compule as seasonal_fruit is private
  // meal.seasonal_fruit = String::from("banana");

  let order1 = back_of_house::Appetizer::Soup;
  let order2 = back_of_house::Appetizer::Salad;

  println!("order1: {:?}, order2: {:?}", order1, order2);

  // hosting is valid here because of: 'use front_of_house::hosting;' line 
  let vege1 = hosting::Vegetable::Tomato;

  println!("vege1: {:?}", vege1);

}

// 2 Result types so need to refer to the parent module
use std::fmt;
use std::io;

fn func1(f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "func1")
}
fn func2() -> io::Result<String> {
    let mut buffer = String::new();
    Ok(buffer)
}

use std::io::Result as IoResult;

fn func3() -> IoResult<String> {
    let mut buffer = String::new();
    Ok(buffer)
}

// public shortcut
pub use crate::front_of_house::serving;

// import HashMap from standard library
use std::collections::HashMap;

// nested use
// same as: 
// use std::cmp::Ordering; 
// use std::env;
use std::{cmp::Ordering, env}; 

// all public items from collections
use std::collections::*;

