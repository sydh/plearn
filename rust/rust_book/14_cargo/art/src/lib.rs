//! # Art
//!
//! A library for modeling artistic concepts

// re export item so user can write:
// use art::PrimaryColor;
// use art::mix;
//
// instead of
//
// use art::kinds::PrimaryColor;
// use art::utils::mix;
//
pub use self::kinds::PrimaryColor;
pub use self::kinds::SecondaryColor;
pub use self::utils::mix;

pub mod kinds {
    
    #[derive(Debug)]
    /// The primary colors according to the RYB color model.
    pub enum PrimaryColor {
        Red,
        Yellow,
        Blue,
    }

    #[derive(Debug)]
    #[derive(PartialEq)]
    /// The secondary colors according to the RYB color model.
    pub enum SecondaryColor {
        Orange,
        Green,
        Purple,
    }
}

pub mod utils {
    use super::kinds::*;

    /// Combines two primary colors in equal amounts to create
    /// a secondary color.
    pub fn mix(c1: PrimaryColor, c2: PrimaryColor) -> SecondaryColor {
        println!("Mixing {:?} + {:?}...", c1, c2);
        SecondaryColor::Orange
    }
}

#[cfg(test)]
mod tests {
    
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(utils::mix(kinds::PrimaryColor::Red, kinds::PrimaryColor::Yellow), kinds::SecondaryColor::Orange);
        assert_eq!(utils::mix(kinds::PrimaryColor::Yellow, kinds::PrimaryColor::Blue), kinds::SecondaryColor::Green);
    }
}

