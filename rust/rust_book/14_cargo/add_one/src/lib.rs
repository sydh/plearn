//! # add_one
//!
//! `add_one` is a collection of utilities to perform complex computation

/// Add two numbers
/// 
/// # Examples
///
/// ```
/// let a = 2;
/// let b = 3;
/// let res = add_one::add_num(a, b);
/// assert_eq!(res, 5);
/// ```

// The example below will be tested when: cargo test

pub fn add_num(a: u32, b: u32) -> u32 {
    a+b
}

#[cfg(test)]
mod tests {
    
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(add_num(2, 2), 4);
    }
}
