#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

pub fn add_two(a: i32) -> i32 {
    a + 2
}

pub fn greetings(_name: &str) -> String {
    // format!("Hello {}", _name)
    format!("Hello")
}

pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Self {
        // if value < 1 || value > 100 {
        // Buggy code 
        if value < 1 {
            panic!("Guess value require to be in range [1, 100], got {}", value);
        }
        println!("Returning Guess struct...");
        Guess { value: value }
    }
}   

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn will_panic() {
        panic!("Make this test fail");
    }

    use super::*;

    #[test]
    fn larger_can_hold_smaller() {
        let larger = Rectangle { width: 10, height: 10 };
        let smaller = Rectangle { width: 9, height: 9 };
        assert!(larger.can_hold(&smaller));
    }

    #[test]
    fn smaller_cannot_hold_larger() {
        let larger = Rectangle { width: 10, height: 10 };
        let smaller = Rectangle { width: 9, height: 9 };
        assert!(!smaller.can_hold(&larger));
    }

    #[test]
    fn it_adds_two() {
        // assert EQual
        assert_eq!(4, add_two(2));
        // assert Not Equal
        assert_ne!(5, add_two(2));
    }

    #[test]
    fn check_greetings() {
        let result = greetings("Bob");
        assert!(
            result.contains("Bob"),
            "Greetings did not contain name, value was `{}`", result
        );
    }

    #[test]
    #[should_panic]
    fn test_guess_gt_100() {
        // Assume code will panic
        // Test will return false as buggy code will not panic here
        Guess::new(200);
    }

    #[test]
    fn it_works_with_result() -> Result<(), String> {
        if 2+2==5 {
            Ok(())
        } else {
            Err(String::from("##### 2+2 != 5!! #####"))
        }
    }
    
    #[test]
    #[ignore]
    fn will_panic2() {
        panic!("Make this test fail");
    }

}
