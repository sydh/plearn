# Title

> chap11: rust book, writing tests

## Install

```
```

## Usage

```
cd adder && cargo test
```

Running test + function output

```
cargo test -- --nocapture
```

Running only a subset of tests

```
cargo test it
```

## Contributing

PRs accepted.

## License

Public domain

