
#[derive(Debug)]
enum IpKind {
  V4,
  V6
}

// 1st version: naive
#[derive(Debug)]
struct IpAddr {
    kind: IpKind,
    address: String,
}

// 2nd version: enum + data
#[derive(Debug)]
enum IpAddr2 {
  V4(String),
  V6(String),
}

// 3rd version: enum + data + multiple types
#[derive(Debug)]
enum IpAddr3 {
  V4(u8, u8, u8, u8),
  V6(String),
}

// another enum with multiple types
#[derive(Debug)]
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) {
        println!("Method call() from {:?}", self);
    }
}

fn main() {

  println!("chap 06: 01_struct");

  let four = IpKind::V4;
  let six = IpKind::V6;
  println!("four: {:?}, six: {:?}", four, six);  

  let localhost1 = IpAddr {
    kind: four,
    address: String::from("127.0.0.1"),
  };

  let loopback1 = IpAddr {
    kind: IpKind::V6,
    address: String::from("::1"),
  };

  println!("localhost1: {:?}", localhost1);  
  println!("loopback1: {:?}", loopback1);  

  let localhost2 = IpAddr2::V4(String::from("127.0.0.1"));
  let loopback2 = IpAddr2::V6(String::from("::1"));

  println!("localhost2: {:?}", localhost2);  
  println!("loopback2: {:?}", loopback2);  

  let localhost3 = IpAddr2::V4(String::from("127.0.0.1"));
  let loopback3 = IpAddr2::V6(String::from("::1"));

  println!("localhost3: {:?}", localhost3);  
  println!("loopback3: {:?}", loopback3);  

  let localhost4 = IpAddr3::V4(127, 0, 0, 1);
  let loopback4 = IpAddr3::V6(String::from("::1"));

  println!("localhost4: {:?}", localhost4);  
  println!("loopback4: {:?}", loopback4);  

  // Note: enum already in std lib
  // See doc:
  // https://doc.rust-lang.org/std/net/enum.IpAddr.html

  // use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
  // let localhost_v4 = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));

  let msg1 = Message::Quit;
  let msg2 = Message::Move {x: 15, y: 25};
  let msg3 = Message::Write(String::from("foo"));
  let msg4 = Message::ChangeColor(255, 0, 0);
  println!("Message {:?}", msg1);
  println!("Message {:?}", msg2);
  println!("Message {:?}", msg3);
  println!("Message {:?}", msg4);
  msg1.call();
  msg3.call();

  // Option
  // No null type in Rust
  // doc here:
  // https://doc.rust-lang.org/std/option/enum.Option.html

  // No need to specify type, auto infered
  let o1 = Some(5);
  // Need to specify the type here 
  let o2: Option<i32> = None;

  println!("o1: {:?}, o2: {:?}", o1, o2);
  println!("o1 is None?: {}, o2 is None?: {}", o1.is_none(), o2.is_none());
  println!("o1 value: {}", o1.unwrap());
  // Warning: will panic!
  // println!("o2 value: {}", o2.unwrap());
  
  // Unwrap with default value
  println!("o2 value: {}", o2.unwrap_or(-1));  
  // Lazy eval: need to specify a lambda func
  println!("o2 value: {}", o2.unwrap_or_else(|| { -1 }));  
}

