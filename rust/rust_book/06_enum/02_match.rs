#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

#[derive(Debug)]
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter,
}

#[derive(Debug)]
enum Coin2 {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

// Note: use ref instead of plain object
fn value_in_cents(coin: &Coin) -> u8 {
    match coin {
        Coin::Penny => {
          println!("It's a penny!");
          1
        },
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}

fn value_in_cents2(coin: &Coin2) -> u8 {
    match coin {
        Coin2::Penny => 1,
        Coin2::Nickel => 5,
        Coin2::Dime => 10,
        Coin2::Quarter(cstate) => { 
          println!("quarter from state: {:?}", cstate);
          25
        },
    }
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        Some(i) => Some(i+1),
        // Note: covering all case is mandatory in Rust
        None => None,
    }
}


fn main() {
    println!("chap 06: 02_match");

    // 
    // match examples

    let c: Coin = Coin::Penny;
    println!("coin c: {:?}", c);
    println!("coin value: {}", value_in_cents(&c));
    println!("coin c: {:?}", c);

    let state = UsState::Alaska;
    let c2 = Coin2::Quarter(state);
    println!("coin c2: {:?}", c2);
    println!("coin value: {}", value_in_cents2(&c2));

    // match with Option
    let five = Some(5);
    let six = plus_one(five);
    println!("{:?} - {:?}", five, six);

    let mut some_u8_value = 0u8;
    println!("some_u8_value: {}", some_u8_value);
    
    match some_u8_value {
        1 => println!("one"),
        7 => println!("seven"),
        // Note: match all other case
        _ => (),
    }
    
    //
    // if let

    let some_u8_v = Some(3);
    match some_u8_v {
        Some(3) => {
            println!("3!!");
        }
        _ => (),
    }

    // short way

    if let Some(3) = some_u8_v { println!("3!!"); }

    // another exemple: if let .. else
    
    let mut count = 0;
    if let Coin2::Quarter(state) = c2 {
      println!("State quarter from {:?}", state);
    }
    else {
        count += 1;
    }
    println!("count: {}", count);   

}
