use std::collections::HashMap;
use std::convert::TryInto;

fn main() {
  // Given a list of integers, use vec and return mean, median and mode

  let mut v: Vec<i32> = vec![1, 2, 3, 4, 5, 2];

  println!("vector v: {:?}", v);

  // mean
  let _v_sum: i32 = v.iter().sum();
  // v.len return type usize so convert into i32 here
  let _v_len: i32 = v.len().try_into().unwrap();
  let v_sum: f64 = _v_sum.try_into().unwrap();
  let v_len: f64 = _v_len.try_into().unwrap();
  println!("v mean: {}", v_sum / v_len);

  // mean: correction
  let v_sum2: i32 = v.iter().sum();
  println!("v mean: {}", v_sum2 as f32 / v.len() as f32);

  // median
  v.sort();
  let v_len: i32 = v.len().try_into().unwrap();
  if v_len % 2 != 0 {
    let _idx = v_len / 2;
    let idx: usize = _idx.try_into().unwrap();
    println!("v median: {:?}", v.get(idx));
  }
  else
  {
    let _idx1 = (v_len - 1) / 2;
    let idx1: usize = _idx1.try_into().unwrap();

    let _idx2 = (v_len) / 2;
    let idx2: usize = _idx2.try_into().unwrap();

    let v1: &i32 = v.get(idx1).unwrap();
    let v2: &i32 = v.get(idx2).unwrap();
    let fv1: f64 = (*v1).try_into().unwrap();
    let fv2: f64 = (*v2).try_into().unwrap();

    println!("v median: {:?}", (fv1+fv2)/2.0 );
  }

  // media correction
  v.sort(); // already sorted but for completness
  let mid = v.len() / 2;
  if v.len() % 2 == 0 {
    let v1 = v[mid -1] as i32;
    let v2 = v[mid] as i32;
    println!("v median: {}", (v1+v2) as f32/2.0);
  } else {
    println!("v median: {}", v[mid]);
  }

  // mode
  let mut hm = HashMap::new();
  let mut v_mode = &v[0];
  let mut v_mode_count = 1;
  for i in v.iter() {
    let count = hm.entry(i).or_insert(0);
    *count+=1;
    if *count > v_mode_count {
      v_mode = i;
      v_mode_count = *count;
    }
  }
  println!("v mode: {}, count: {}", v_mode, v_mode_count); 
}
