use std::fmt;
use std::fmt::Display;

struct S {
 a: i32,
 b: f64,
}

impl Display for S {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "struct, a: {}, b: {}", self.a, self.b)
  }
}

fn main() {

  // Create string

  let mut s = String::new();

  println!("s: {}", s);

  // specify types to highlight them
  let data2: &str = "foo bar";
  let s2: String = data2.to_string();

  let data3 = S { a: 19, b: 0.78154 };
  // This works because struct S has the trait Display
  let s3: String = data3.to_string();

  println!("s2: {}", s2);
  println!("s3: {}", s3);

  // UTF-8 tests
  let hello = String::from("Hello");
  println!("hello: {}", hello);
  let hello = String::from("שָׁלוֹם");
  println!("hello: {}", hello);
  let hello = String::from("नमस्ते");
  println!("hello: {}", hello);

  // String methods

  let mut ms1 = String::from("Hello");
  // push a string
  ms1.push_str(" world");
  // push a single char
  ms1.push('!');
  println!("ms1: {}", ms1);

  let ms2_0 = String::from("foo2");
  let ms2_1 = String::from("bar2");
  let ms2 = ms2_0 + " " + &ms2_1;
  println!("ms2: {:#?}", ms2);
  // ms2_0 has been dropped, ms2_1 is still avail 
  // println!("ms2_0: {:#?}", ms2_0);
  println!("ms2_1: {:#?}", ms2_1);

  let fs1 = String::from("tic");
  let fs2 = String::from("tac");
  let fs3 = String::from("toe");
  let fs: String = format!("{}-{}-{}", fs1, fs2, fs3);
  println!("fs: {}", fs);

  

}
