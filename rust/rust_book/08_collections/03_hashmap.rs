use std::collections::HashMap;

fn main() {

  // Creating HashMap

  let mut scores = HashMap::new();

  scores.insert(String::from("Blue"), 10);
  scores.insert(String::from("Red"), 50);

  println!("scores: {:?}", scores);

  let teams = vec![
    String::from("Team 1"),
    String::from("Team 2"),
  ];
  let init_scores = vec![25, 60];
  // let Rust infer types for HashMap
  let scores2: HashMap<_, _> = teams.iter().zip(init_scores.iter()).collect();
  // partial infer
  let _scores2: HashMap<&String, _> = teams.iter().zip(init_scores.iter()).collect();
  // no infer
  let __scores2: HashMap<&String, &i32> = teams.iter().zip(init_scores.iter()).collect();

  // just playing with iterator here...
  println!("{:?}", teams.iter());
  for (i, j) in teams.iter().zip(init_scores.iter()) {
    println!("i: {}, j: {}", i, j);
  }

  println!("scores2: {:?}", scores2);

  // HashMap and ownership
  let field_name = String::from("Favorite color");
  let field_value = String::from("Orange");

  let mut hm1: HashMap<String, String> = HashMap::new();
  hm1.insert(field_name, field_value);

  // Will not compile, hm1 is owner of field_name value
  // println!("field_name: {}", field_name);

  // Accessing value in HashMap

  let mut hm2 = HashMap::new();
  hm2.insert(0, 52);
  hm2.insert(1, 27);
  println!("hm2 element 0: {:?}", hm2.get(&0));
  match hm2.get(&1) {
    Some(v) => println!("hm2 element 1: {}", v),
    None => (),
  } 

  // Note: use ref here otherwise borrowed then dropped
  for (key, value) in &scores {
    println!("{} ==> {}", key, value);
  }

  // Updating an HashMap
  
  scores.insert(String::from("Red"), 78); 
  println!("scores: {:?}", scores);
  // only insert a value if the key has no value
  scores.entry(String::from("Red")).or_insert(15);
  // create entry Yellow in HashMap then insert value
  scores.entry(String::from("Yelllow")).or_insert(15);
  // Will do nothing here
  scores.entry(String::from("Green"));
  println!("scores: {:?}", scores);
  
  // Updating a value based on the old value

  let text = "Hello Mr. Foo Bar Bar";
 
  // Specify type is not mandatory here, can be infered
  // let mut m_text: HashMap<&str, i32> = HashMap::new();
  let mut m_text: HashMap<&str, u32> = HashMap::new();
  // https://doc.rust-lang.org/std/str/struct.SplitWhitespace.html
  for word in text.split_whitespace() {
    println!("word: {}", word);
    // https://doc.rust-lang.org/std/collections/struct.HashMap.html#method.entry
    //https://doc.rust-lang.org/std/collections/hash_map/enum.Entry.html#method.or_insert
    // entry return a type: Entry 
    // Entry.or_insert return a mut ref to the value of the entry
    let count = m_text.entry(word).or_insert(0);
    *count += 1;
  }
  println!("m_text: {:?}", m_text);


}
