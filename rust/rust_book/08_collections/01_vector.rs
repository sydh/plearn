fn main() {

  // Creating Vector

  // create an empty vector to hold value of type i32
  // note: Vec is defined using generics -> Vec<T> 
  // so Rust requires to define the type of values
  let v: Vec<i32> = Vec::new();
  println!("v: {:?}", v);

  let v2 = vec![1, 2, 3];
  let _v2 = vec!(1, 2, 3, 4, 5);
  // pretty print
  println!("v2: {:#?}", v2);
  println!("_v2: {:?}", _v2);
    
  let mut v3 = Vec::new();
  v3.push(5);
  v3.push(9);
  v3.push(12);

  println!("v3: {:#?}", v3);

  // Reading Vectors
  let first: i32 = v3[0];
  // auto infer type here
  let first1 = v3[0];
  let second: &i32 = &v3[1];
  println!("item 0 and 1: {}, {}", first, second);
  // Vec.get return an Option<&T> 
  // see: https://doc.rust-lang.org/std/vec/struct.Vec.html#method.get
  match v3.get(2) {
    Some(third) => println!("The 3rd element is {}", third),
    None => println!("There is no third element"),
  }

  // get can also return a slice
  match v3.get(..2) {
    Some(sl) => println!("The 1st and 2nd element is {:?}", sl),
    None => println!("There is no first nor second element"),
  }

  // accessing elements beyond vector size
  
  // error:
  // thread 'main' panicked at 'index out of bounds: the len is 3 but the index is 100'
  // let _i100 = v3[100];

  // adding an element while holding a ref
  let mut v4 = vec![1, 2, 3, 4, 5];
  let v4_first = &v4[0];
  v4.push(6);
  // immutable borrow occurs at the previous line (v4.push(...))
  // println!("The first element of v4: {}", v4_first);

  // iter over vector
  
  println!("== iter over v3 ==");
  for i in &v3 {
    println!("item: {}", i);
  }

  let mut v5 = vec![9, 9, 10];
  for i in &mut v5 {
    // i is a mutable ref of i32
    // deference i to get value
    *i += 70;
  }
  println!("v5: {:?}", v5);

  // Enum + Vector


  #[derive(Debug)]
  enum SpreadsheetCell {
    Int(i32),
    Float(f32),
    Text(String),
  }

  let mut v6: Vec<SpreadsheetCell> = Vec::new();
  v6.push(SpreadsheetCell::Int(8));
  v6.push(SpreadsheetCell::Text(String::from("Yo")));
  println!("v6: {:?}", v6);

  // test some more function from documentation
  // https://doc.rust-lang.org/std/vec/struct.Vec.html

  let mut vstack = Vec::new();
  vstack.push("AA");
  vstack.push("BB");
  vstack.push("CC");

  while let Some(i) = vstack.pop() {
    println!("item: {}", i);
  }

  // Vec.iter

  let v = vec![15.25, 89.76, 74.123];
  for i in v.iter() {
    println!("i: {}", i);
  }

}
