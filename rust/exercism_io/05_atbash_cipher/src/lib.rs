#[macro_use]
extern crate lazy_static;

use std::collections::HashMap;

const PLAIN: [char; 26] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
];

lazy_static! {
    static ref CHAR_MAP: HashMap<char, char> = build_char_map();
}

fn build_char_map() -> HashMap<char, char> {
    PLAIN
        .iter()
        .zip(PLAIN.iter().rev())
        .map(|(c1, c2)| {
            (*c1, *c2) 
        })
        .collect()
}

fn cipher_begin(plain: &str) -> impl Iterator<Item=char> + '_ {
    
    plain
        .chars()
        .filter(|c| c.is_ascii_alphanumeric())
        .map(|c| {
            let c2 = c.to_ascii_lowercase();
            *CHAR_MAP.get(&c2).unwrap_or_else(|| &c2)
        })
}


/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {

    const CHUNK: usize = 5;
    cipher_begin(plain)
        .enumerate()
        .flat_map(|(i, c)| {
            if i != 0 && i % CHUNK == 0 {
                Some(' ')
            } else {
                None
            }
            .into_iter()
            .chain(std::iter::once(c))
        })
        .collect::<String>()
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    cipher_begin(cipher)
        .collect::<String>()
}

