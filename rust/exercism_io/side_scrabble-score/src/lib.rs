#[macro_use]
extern crate lazy_static;

use std::collections::HashMap;

static SCORE_RULES: [(&'static str, u8); 7] = [
    ("aeioulnrst", 1),
    ("dg", 2),
    ("bcmp", 3),
    ("fhvwy", 4),
    ("k", 5),
    ("jx", 8),
    ("qz", 10)
];

// Note: using lazy_static, the hashmap build func will only be called onced!
lazy_static! {
    static ref CHAR_MAP: HashMap<char, u8> = build_char_map();
}

fn build_char_map() -> HashMap<char, u8> {
    SCORE_RULES
        .iter()
        .flat_map(|&(letters, score)| {
            letters
                .chars()
                // force the closure to take ownership of score
                // compiler suggests this
                .map(move |letter| {
                    (letter, score)
                })
        })
        .collect()
}


/// Compute the Scrabble score for a word.
pub fn score(word: &str) -> u64 {

    // for each char, get value from CHAR_MAP & sum
    word
        .to_lowercase()
        .chars()
        .map(|c| {
            let _v = CHAR_MAP.get(&c);
            match _v {
                Some(v) => *v as u64,
                None => 0,
            }
        }).sum()
}
