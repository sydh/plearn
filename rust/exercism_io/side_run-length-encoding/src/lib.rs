fn push_chars(counter: i32, c: char, v: &mut Vec<String>) {
    let mut s = String::new();
    if counter > 1 {
        s.push_str(&counter.to_string());
    }
    s.push(c);
    v.push(s);
}

pub fn encode(source: &str) -> String {
    // XXX: accumulate iterator eg. iter<2, 'A'>, iter<3, 'B'> and collect at the end?
    // this could help to alloc only once by first iterating over iterators?
    let mut iter = source.chars();

    let mut r: Vec<String> = Vec::new();
    let mut c_last = '@';
    let mut counter = 0;

    loop {
        let oc = iter.next();
        match oc {
            Some(c) => {
                if c_last == c {
                    counter += 1;
                } else {
                    if c_last != '@' {
                        push_chars(counter + 1, c_last, &mut r);
                    }

                    counter = 0;
                    c_last = c;
                }
            }
            None => {
                // last push if things remain in stack
                if c_last != '@' {
                    push_chars(counter + 1, c_last, &mut r);
                }

                break;
            }
        }
    }

    r.join("")
}

pub fn decode(source: &str) -> String {
    let mut iter = source.chars();
    let mut count_s = String::new();
    let mut c_last = '@';
    let mut s = String::new();
    loop {
        let oc = iter.next();
        match oc {
            Some(c) => {
                if c.is_numeric() {
                    count_s.push(c);
                } else {
                    if c_last != c {
                        let mut count = 1;
                        if count_s.is_empty() == false {
                            count = count_s.parse::<i32>().unwrap();
                        }
                        // change of character - time to write
                        while count > 0 {
                            s.push(c);
                            count -= 1;
                        }
                    }
                    count_s.clear();
                    c_last = c;
                }
            }
            None => {
                break;
            }
        }
    }

    s
}
