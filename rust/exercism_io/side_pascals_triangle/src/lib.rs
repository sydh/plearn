use std::cmp;
use std::iter;

pub struct PascalsTriangle {
    row_count: usize,
}

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        PascalsTriangle { row_count: row_count as usize } 
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {

        // Ideas from 
        // https://exercism.io/tracks/rust/exercises/pascals-triangle/solutions/f9e9e3a216334ae79e686657633e0954
        // Modified version to avoid:
        // 1. recursion
        // 2. multiple memory alloc

        // Note on cloned() calls
        // cloned() simply copies an integer each time you get a value out of the iterator
        // it DOES NOT need to alloc memory on the heap
        // Note that zip1 and zip2 use iter() as it does not consume the row_prev
        // thus the need for cloned()

        let mut rows: Vec<Vec<u32>> = Vec::with_capacity(self.row_count);

        if self.row_count > 0 {
            rows.push(vec![1]);
        }
        (1..self.row_count).for_each(|i| {
            let mut row = Vec::with_capacity(i+1);
            let row_prev = &rows[i-1];
            // eg. previous row: [1, 1]
            // zip1: [0, 1, 1]
            let zip1 = iter::once(0).chain(row_prev.iter().cloned());
            // zip2: [1, 1, 0]
            let zip2 = row_prev.iter().cloned().chain(iter::once(0));
            // result: [1, 2, 1]
            // extend() here instead of collect() to avoid growing progessively
            row.extend(zip1.zip(zip2).map(|(n, m)| n+m));
            rows.push(row);
        });
        rows
    }

    pub fn rows1(&self) -> Vec<Vec<u32>> {
        
        // old solution

        let mut rows: Vec<Vec<u32>> = Vec::with_capacity(self.row_count);

        for i in 0..self.row_count {
            let mut row = vec![1; i+1];
            for j in 0..i {
                let mut start_index = 0;
                if j > 0 {
                    start_index = j-1;
                }
                let end_index = cmp::min(j, rows[i-1].len()) + 1;
                let row_prev = &rows[i-1];
                let row_prev_slice = &row_prev[start_index..end_index];
                let sum_r: u32 = row_prev_slice.iter().sum();
                if sum_r == 0 {
                    row[j] = 1
                } else {
                    row[j] = sum_r;
                }
            }

            rows.push(row);
        }

        rows
    }
}

