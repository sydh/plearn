use std::collections::HashMap;

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {

    let mut ncount = 0;
    let cl = |c| {
        match c {
            'A' | 'C' | 'G' | 'T' => Ok(c),
            _ => Err(c),
        }
    };

    if cl(nucleotide).is_err()
    {
        return Err(nucleotide);
    }

    for c in dna.chars() {
        match cl(c) {
            Ok(_) => {
                if c == nucleotide {
                    ncount += 1;
                }
            },
            Err(ch) => return Err(ch),
        } 
    }

    Ok(ncount)
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let mut h: HashMap<char, usize> = "ACGT".chars().map(|c| (c, 0)).collect();
    for c in dna.chars() {
        match c {
            'A' | 'C' | 'G' | 'T' => {
                h.entry(c)
                    .and_modify(|v| *v += 1);
            },
            _ => { return Err(c); },
        }
    }
    Ok(h)
}
