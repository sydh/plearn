use regex::Regex;

pub fn translate_word(word: &str) -> String {
    
    let mut w: String = String::from(word);

    // println!("word {}", w);

    let regexes = [
        // rule 1
        Regex::new(r"^([aeiou]+|xr|yt)").unwrap(),
        // rule 3
        Regex::new(r"^(([^aeiou]+)?qu)").unwrap(),
        // rule 4
        Regex::new(r"^([^aeiou]+y)").unwrap(),
        // rule 2
        Regex::new(r"^([^aeiou]+)").unwrap(),
    ];     
    
    for (i, rgx) in regexes.iter().enumerate() {
        // println!("i {} - rgx {:?}", i, rgx);
        
        match rgx.captures(word) {
            Some(caps) => {
                // println!("caps {:?}", caps);
                let part1 = caps.get(1).unwrap().as_str();
                // println!("part1: {}", part1);
                match i {
                     1 | 3 => {
                         w = String::from(&word[part1.len()..]);
                         w.push_str(part1);
                     },
                     2 => {
                         let p1len = part1.len() - 1;
                         w = String::from(&word[p1len..]);
                         w.push_str(&part1[..p1len]); 
                     }
                     _ => (),
                }
                // break as soon as one regex match
                break;
            },
            _ => (),
        }
    }

    // all rule need that
    w.push_str("ay");
    w
}

pub fn translate(input: &str) -> String {

    // let mut res: Vec<String> = Vec::new();
    //
    // for word in input.split(' ') {
    //     res.push(translate_word(word));
    // }
    //
    // res.join(" ")

    input
        .split_whitespace()
        .map(translate_word)
        .collect::<Vec<String>>()
        .join(" ")

}

pub fn translate1(input: &str) -> String {
    input
        .split_whitespace()
        .map(latinize)
        .collect::<Vec<_>>()
        .join(" ")
}

fn latinize(word: &str) -> String {
    let mut pos = word
        .find(|c| "aeiou".contains(c))
        .or(word.find('y'))
        .unwrap();

    if pos > 0 && &word[pos - 1..=pos] == "qu" {
        pos += 1;
    }

    if &word[..2] == "yt" || &word[pos..] == "ay" {
        pos = 0;
    }

    format!("{}{}ay", &word[pos..], &word[..pos])
}

